package pl.lodz.p.it.ssbd2016.ssbd02.mos.endpoints;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.City;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.University;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.CityException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.UniversityException;

import java.util.List;

/**
 * Created by Joanna Cichecka on 2016-05-07.
 */
public interface MOSEndpointLocal {

    /**
     * Przeprowadza dodawanie miasta.
     * @param city dodawane miasto
     * @throws ApplicationException
     */
    void addCity(City city) throws ApplicationException;

    /**
     * Przeprowadza edycję miasta.
     * @param newCity edytowane miasto
     * @param oldCity miasto do edycji
     * @throws ApplicationException
     */
    void editCity(City newCity, City oldCity) throws ApplicationException;

    /**
     * Przeprowadza pobranie z bazy listy wszystkich miast.
     * @return lista wszystkich miast z bazy danych
     * @throws ApplicationException
     */
    List<City> getCities() throws ApplicationException;

    /**
     * Przeprowadza dodawanie uczelni.
     * @param university dodawana uczelnia
     * @throws ApplicationException
     */
    void addUniversity(University university) throws ApplicationException;

    /**
     * Przeprowadza edycję uczelni
     * @param newUniversity nowa uczelnia
     * @param oldUniversity stara uczelnia
     * @throws ApplicationException
     */
    void editUniversity(University newUniversity, University oldUniversity) throws ApplicationException;

    /**
     * Przeprowadza pobranie z bazy listy wszystkich uczelni.
     * @return lista wszystkich uczelni z bazy danych
     * @throws ApplicationException
     */
    List<University> getUniversities() throws ApplicationException;
    University findUniversityByName(String name) throws UniversityException;

    /**
     * Pobiera z bazy miasto o podanej nazwie
     * @param name nazwa miasta
     * @return obiekt City o podanej nazwie
     * @throws CityException
     */
    City getCity(String name) throws CityException;

    /**
     * Pobiera z bazy uczelnię o podanym Id
     * @param id id uczelni
     * @return obiekt University o podanym id
     * @throws UniversityException
     */
    University getUniversityById(Short id) throws UniversityException;

    /**
     * Sprawdza czy istnieje w bazie miasto o podanej nazwie. Jeśli nie, dodaje je.
     * @param cityName nazwa miasta
     * @return obiekt City o podanej nazwie
     * @throws ApplicationException
     */
    City getOrCreateCity(String cityName) throws ApplicationException;
}
