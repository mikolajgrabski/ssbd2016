package pl.lodz.p.it.ssbd2016.ssbd02.mpo.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Advert;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.City;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.mpo.endpoints.MPOEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import java.util.List;

/**
 * Created by Joanna Cichecka on 2016-06-12.
 * Bean wyszukujący ogłoszenia
 */
@Named("searchPageBean")
@RequestScoped
public class SearchPageBean {
    private final static String OUTCOME_PATH = "search-results";

    @EJB
    private MPOEndpointLocal mpoEndpoint;

    private String errorMessage;
    private boolean error = false;
    private Text propertiesDict;

    /**
     * Query elements:
     */
    private String phrase;
    private String cityName;

    private DataModel<Advert> advertDataModel;
    private List<City> cityList;

    /**
     * Inicjalizuje zmienne, pobierając wszystkie miasta do bazy
     */
    @PostConstruct
    public void init() {
        phrase = "";
        cityName = "";
        try {
            cityList = mpoEndpoint.getCities();
        } catch (ApplicationException e) {
            addOperationMessage(true, "citylistempty");
        }
    }

    /**
     * Wyszukuje ogłoszenie - pobiera listę ogłoszeń
     * @return
     */
    public String search() {
        try {
            advertDataModel = new ListDataModel<>(mpoEndpoint.searchForAdvert(phrase, cityName));
        } catch (ApplicationException e) {
            addOperationMessage(true, "noresultlist");
        }
        return OUTCOME_PATH;
    }

    public DataModel<Advert> getAdvertDataModel() {
        return advertDataModel;
    }

    public void setAdvertDataModel(DataModel<Advert> advertDataModel) {
        this.advertDataModel = advertDataModel;
    }

    public List<City> getCityList() {
        return cityList;
    }

    public void setCityList(List<City> cityList) {
        this.cityList = cityList;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public Text getPropertiesDict() {
        return propertiesDict;
    }

    public void setPropertiesDict(Text propertiesDict) {
        this.propertiesDict = propertiesDict;
    }

    /**
     * Dodanie komunikat o wyniku operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Nazwa property, której wartość wyświetlana jest użytkownikowi
     */
    private void addOperationMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, propertiesDict.getString(detail));
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
