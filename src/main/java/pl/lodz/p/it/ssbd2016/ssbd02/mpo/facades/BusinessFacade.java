/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mpo.facades;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AbstractFacade;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Business;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;

/**
 * Fasada do zarządzania biznesen
 * @author Patryk
 */
@Stateless(name = "BussinessFacadeMPO")
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class BusinessFacade extends AbstractFacade<Business> implements BusinessFacadeLocal {

    @PersistenceContext(unitName = "ssbd02mpoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BusinessFacade() {
        super(Business.class);
    }
    
}
