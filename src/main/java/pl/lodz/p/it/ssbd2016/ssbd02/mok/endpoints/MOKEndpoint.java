package pl.lodz.p.it.ssbd2016.ssbd02.mok.endpoints;

import com.google.common.hash.Hashing;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.*;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.EmailException;
import pl.lodz.p.it.ssbd2016.ssbd02.mok.facades.AccountFacadeLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.mok.managers.emailmanager.EmailGenerator;
import pl.lodz.p.it.ssbd2016.ssbd02.mok.managers.emailmanager.EmailManagerLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.utils.CloneUtils;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.Resource;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.*;
import javax.interceptor.Interceptors;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateful
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class MOKEndpoint implements MOKEndpointLocal, SessionSynchronization {

    private static final Logger logger = Logger.getLogger(MOKEndpoint.class.getName());

    @EJB
    private AccountFacadeLocal accountFacade;
    @EJB
    private EmailManagerLocal emailManager;
    private Account currentAccount;
    private long transactionId;
    @Resource
    private SessionContext sctx;

    @Override
    @RolesAllowed("activateAccount")
    public void activateAccount(Account account) throws AccountException, EmailException {
        Account acc = accountFacade.find(account.getId());
        acc.setActive(true);
        acc.setFailedAttempts(Short.parseShort("0"));
        accountFacade.edit(acc);

        emailManager.sendEmail(account, EmailGenerator.EmailType.ACCOUNT_UNLOCKED);
    }

    @Override
    @RolesAllowed("deactivateAccount")
    public void deactivateAccount(Account account) throws AccountException, EmailException {
        Account acc = accountFacade.find(account.getId());
        acc.setActive(false);
        accountFacade.edit(acc);

        emailManager.sendEmail(account, EmailGenerator.EmailType.ACCOUNT_BLOCKED);
    }

    @Override
    @PermitAll
    public void registerAccount(Account account) throws AccountException, EmailException {
        try {
            String password = Hashing.sha256().hashString(account.getPassword(), StandardCharsets.UTF_8).toString();
            account.setPassword(password);

            List<AccessLevel> accessLevels = new ArrayList<>();
            AccessLevel accessLevel = new AccessLevel();
            accessLevel.setAccountId(account);
            accessLevel.setLevel(RoleType.valueOf(account.getProfileType().toString()));
            accessLevel.setActive(true);
            accessLevels.add(accessLevel);
            account.setAccessLevelList(accessLevels);
            accountFacade.create(account);

            createProfile(accountFacade.findByLogin(account.getLogin()));


        } catch (IllegalArgumentException | AccountException e) {
            throw e;
        }
        emailManager.sendEmail(account, EmailGenerator.EmailType.ACCOUNT_CREATED);
    }

    @TransactionAttribute(TransactionAttributeType.MANDATORY)
    private void createProfile(Account account) throws AccountException {
        Text dictionary = new Text();
        if(account.isEmployer() || account.isEmployee()) {
            try {
                if (account.isEmployer()) {
                    Employer profile = new Employer();
                    profile.setName(dictionary.getString("fillme"));
                    profile.setId(account.getId());
                    account.setEmployer(profile);
                } else {
                    Employee profile = new Employee();
                    profile.setFirstName(dictionary.getString("fillme"));
                    profile.setLastName(dictionary.getString("fillme"));
                    profile.setPhoneNumber(dictionary.getString("fillme"));
                    profile.setId(account.getId());
                    account.setEmployee(profile);
                }
                accountFacade.edit(account);

            } catch (ApplicationException e) {
                throw AccountException.createProfileNotCreatedException(e, account);
            } catch (Exception e) {
                throw AccountException.createUnknownErrorException();
            }
        }
    }

    @Override
    @RolesAllowed("createAccount")
    public void createAccount(Account account) throws AccountException {
        String password = Hashing.sha256().hashString(account.getPassword(), StandardCharsets.UTF_8).toString();
        account.setPassword(password);
        account.setActive(true);

        List<AccessLevel> accessLevels = new ArrayList<>();
        AccessLevel accessLevel = new AccessLevel();
        accessLevel.setAccountId(account);
        Text dictionary = new Text();

        if (account.isAdmin()) {
            accessLevel.setLevel(RoleType.ADMIN);
            accessLevel.setActive(true);
            accessLevels.add(accessLevel);
        }
        if (account.isModerator()) {
            accessLevel = new AccessLevel();
            accessLevel.setAccountId(account);
            accessLevel.setLevel(RoleType.MODERATOR);
            accessLevel.setActive(true);
            accessLevels.add(accessLevel);
        }
        if (account.isEmployee()) {
            accessLevel = new AccessLevel();
            accessLevel.setAccountId(account);
            accessLevel.setLevel(RoleType.EMPLOYEE);
            accessLevel.setActive(true);
            accessLevels.add(accessLevel);
        }
        if (account.isEmployer()) {
            accessLevel = new AccessLevel();
            accessLevel.setAccountId(account);
            accessLevel.setLevel(RoleType.EMPLOYER);
            accessLevel.setActive(true);
            accessLevels.add(accessLevel);
        }

        account.setAccessLevelList(accessLevels);
        accountFacade.create(account);

        createProfile(accountFacade.findByLogin(account.getLogin()));
    }

    @Override
    @PermitAll
    public Account getAccountByLogin(String login) throws AccountException {
        return accountFacade.findByLogin(login);
    }

    @Override
    @RolesAllowed("getCurrentAccount")
    public Account getCurrentAccount(Account account) throws IOException, ClassNotFoundException, AccountException {
        currentAccount = accountFacade.find(account.getId());
        return (Account) CloneUtils.deepCloneThroughSerialization(currentAccount);
    }

    @Override
    @PermitAll
    public Account getMyAccount() throws AccountException {
        String callerName = sctx.getCallerPrincipal().getName();
        currentAccount = !callerName.equals("ANONYMOUS") ? accountFacade.findByLogin(callerName) : null;
        return currentAccount;
    }

    @Override
    @RolesAllowed("getCurrentAccessLevels")
    public Collection<AccessLevel> getCurrentAccessLevels(Account account)
            throws NoSuchMethodException, IllegalAccessException, InstantiationException, IOException,
            InvocationTargetException, ClassNotFoundException, AccountException {
        currentAccount = getCurrentAccount(account);
        return (List<AccessLevel>) CloneUtils.deepCloneList(currentAccount.getAccessLevelList());
    }

    @Override
    @RolesAllowed("changeAccessLevels")
    public void changeAccessLevels(Account account) throws AccountException {
        currentAccount = account;

        // Sprawdzenie czy podany obiekt nie jest pusty
        if (account.getAccessLevelList() == null)
            throw new IllegalArgumentException("No access levels collection provided for modification");

        accountFacade.edit(currentAccount);
    }

    @Override
    @RolesAllowed("getAllAccounts")
    public List<Account> getAllAccounts() throws AccountException {
        return accountFacade.findAll();
    }

    /**
     * Umożliwia zmianę loginu lub adresu e-mail konta (lub obu).
     *
     * @param oldAccount stare, dotychczas aktualne konto
     * @param newAccount uzupełnione dane z formularza
     */
    @Override
    @RolesAllowed("editAccount")
    public void editAccount(Account oldAccount, Account newAccount) throws AccountException {
        // oldAccount -- przydatne gdy chcemy sprawdzić unikalność danych
        final String newEmail = newAccount.getEmail();
        final String newLogin = newAccount.getLogin();

        if (newEmail != null) currentAccount.setEmail(newEmail);
        if (newLogin != null) currentAccount.setLogin(newLogin);
        accountFacade.edit(currentAccount);
    }

    @Override
    @PermitAll
    public void editLoginStats(Account account) throws AccountException, EmailException {
        boolean wasActive = accountFacade.findByLogin(account.getLogin()).getActive();

        accountFacade.edit(account);

        if (!account.getActive() && account.getActive() != wasActive) {

            emailManager.sendEmail(account, EmailGenerator.EmailType.ACCOUNT_BLOCKED);
        }
    }

    @Override
    @RolesAllowed("changePassword")
    public void changePassword(String oldPassword, String newPassword) throws AccountException {
        String oldPasswordHash = Hashing.sha256().hashString(oldPassword, StandardCharsets.UTF_8).toString();
        if (currentAccount.getPassword().equals(oldPasswordHash)) {
            currentAccount.setPassword(Hashing.sha256().hashString(newPassword,
                    StandardCharsets.UTF_8).toString());
            accountFacade.edit(currentAccount);
        } else {
            throw AccountException.createIncorrectPasswordException();
        }
        currentAccount = null;
    }

    @Override
    @RolesAllowed("changeUserPassword")
    public void changeUserPassword(Account account, String newPassword) throws AccountException {
        account.setPassword(Hashing.sha256().hashString(newPassword,
                StandardCharsets.UTF_8).toString());
        accountFacade.edit(account);
    }

    @Override
    public void afterBegin() throws EJBException, RemoteException {
        transactionId = System.currentTimeMillis();
        logger.log(Level.SEVERE, "Transaction (ID:" + transactionId + ") has " +
                "started by user with ID: " + (currentAccount != null ? currentAccount.getId() : "GUEST") + ".");
    }

    @Override
    public void beforeCompletion() throws EJBException, RemoteException {
        logger.log(Level.SEVERE, "Transaction (ID:" + transactionId + ") " +
                "started by user with ID: " + (currentAccount != null ? currentAccount.getId() : "GUEST") +
                " before completion.");
    }

    @Override
    public void afterCompletion(boolean committed) throws EJBException, RemoteException {
        logger.log(Level.SEVERE, "Transaction (ID:" + transactionId + ") " +
                "started by user with ID: " + (currentAccount != null ? currentAccount.getId() : "GUEST") +
                " was ended because of: " + (committed ? "COMMIT." : "ROLLBACK."));
    }
}
