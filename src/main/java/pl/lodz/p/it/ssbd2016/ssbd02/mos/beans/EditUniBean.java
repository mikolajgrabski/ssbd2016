package pl.lodz.p.it.ssbd2016.ssbd02.mos.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.City;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.University;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.CityException;
import pl.lodz.p.it.ssbd2016.ssbd02.mos.endpoints.MOSEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.utils.CloneUtils;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Joanna Cichecka on 2016-06-12.
 */
@Named("editUniBean")
@ViewScoped
public class EditUniBean implements Serializable{

    @EJB
    MOSEndpointLocal mosEndpoint;

    private Text propertiesDict;
    private boolean error = false;
    private String errorMessage;
    private University university;
    private University universityToEdit;
    private City city;

    @PostConstruct
    public void init() {
        propertiesDict = new Text();
        city = new City();
        universityToEdit = (University) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("universityToEdit");
        try {
            university = (University) CloneUtils.deepCloneThroughSerialization(universityToEdit);
        } catch (IOException | ClassNotFoundException e) {
            setError(true);
            setErrorMessage(e.getMessage());
            Logger.getLogger(EditUniBean.class.getName()).log(Level.SEVERE, null, e);
            addMessage(true, e.getMessage());
        }
    }

    /**
     * Metoda umożliwiająca edycję uczelni.
     */
    public void edit() {
        try {
            mosEndpoint.editUniversity(university, universityToEdit);
            addOperationMessage(false, "universityedited");
        } catch (ApplicationException e) {
            setError(true);
            setErrorMessage(e.getMessage());
            Logger.getLogger(EditUniBean.class.getName()).log(Level.SEVERE, null, e);
            addMessage(true, e.getMessage());
        }
    }

    public University getUniversity() {
        return university;
    }

    public void setUniversity(University university) {
        this.university = university;
    }

    private void setError(boolean error) {
        this.error = error;
    }

    public boolean isError() {
        return error;
    }

    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Dodanie komunikat o wyniku operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Nazwa property, której wartość wyświetlana jest użytkownikowi
     */
    private void addOperationMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, propertiesDict.getString(detail));
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    /**
     * Dodanie komunikatu w przypadku niepowodzenia operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Wiadomość ze złapanego wyjątku
     */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
