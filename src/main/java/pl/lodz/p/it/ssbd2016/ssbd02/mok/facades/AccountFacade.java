/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mok.facades;

import java.util.List;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.*;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AbstractFacade;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Account;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;

/**
 *
 * @author Patryk
 */
@Stateless(name = "AccountFacadeMOK")
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class AccountFacade extends AbstractFacade<Account> implements AccountFacadeLocal {

    private List<Account> accounts;
    
    @PersistenceContext(unitName = "ssbd02mokPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AccountFacade() {
        super(Account.class);
    }
    
    @Override
    @PermitAll
    public Account findByLogin(String login) throws AccountException{
        try {
            Query q = em.createNamedQuery("Account.findByLogin");
            q.setParameter("login", login);
            return (Account) q.getSingleResult();
        } catch(NoResultException ex) {
            throw AccountException.createNoEntityAccountException();
        }
    }
    
    @Override
    @RolesAllowed("getAllAccounts")
    public List<Account> findAll() throws AccountException {
        accounts = em.createNamedQuery("Account.findAll").getResultList();
        if(accounts.isEmpty()){
            throw AccountException.createNoEntitiesAccountException();
        } else
            return accounts;   
    }
    
    @Override
    @PermitAll
    public void create(Account account) throws AccountException {
        try {
            em.persist(account);
            em.flush();
        } catch (TransactionRequiredException ex) {
            throw AccountException.createTransactionNotExistsException(ex, account);
        } catch(PersistenceException pe) {
            if(pe.getMessage().contains("login_unique")) {
                throw AccountException.createNotUniqueLoginAccountException(pe, account);
            }
            if(pe.getMessage().contains("email_unique")) {
                throw AccountException.createNotUniqueEmailAccountException(pe, account);
            }
            else {
                throw AccountException.createUnknownErrorException();
            }
        }
    }
    
    @Override
    @RolesAllowed({"deactivateAccount", "activateAccount", "getCurrentAccount"})
    public Account find(Object id) throws AccountException {        
        try {    
            Query query = em.createNamedQuery("Account.findById");
            query.setParameter("id", id);
            return (Account) query.getSingleResult();
        } catch(NoResultException ex) {
            throw AccountException.createNoEntityAccountException();
        }
    }
    
    @Override
    @PermitAll
    public void edit(Account account) throws AccountException {
        try {
            em.merge(account);
            em.flush();
        } catch(OptimisticLockException oe) {
            throw AccountException.createAccountEditionAccountException(oe, account);
        } catch(TransactionRequiredException ex) {
            throw AccountException.createTransactionNotExistsException(ex,account);
        } catch(PersistenceException pe) {
            if(pe.getMessage().contains("login_unique")) {
                throw AccountException.createNotUniqueLoginAccountException(pe, account);
            }
            if(pe.getMessage().contains("email_unique")) {
                throw AccountException.createNotUniqueEmailAccountException(pe, account);
            }
            else {
                throw AccountException.createUnknownErrorException();
            }
        }
    }
}
