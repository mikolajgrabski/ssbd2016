package pl.lodz.p.it.ssbd2016.ssbd02.mos.beans;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.City;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.CityException;
import pl.lodz.p.it.ssbd2016.ssbd02.mos.endpoints.MOSEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

@Named("createCityPageBean")
@RequestScoped
public class CreateCityPageBean {
	
	@EJB
    private MOSEndpointLocal mosEndpoint;
	private City city;
	private String errorMessage;
    private boolean error = false;
    private Text propertiesDict;

    /**
	 * Domyślny konstruktor Beana, inicjalizuje obiekty typów Text i City.
	 */
    public CreateCityPageBean() {
        propertiesDict = new Text();
        city = new City();
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public Text getPropertiesDict() {
		return propertiesDict;
	}

	public void setPropertiesDict(Text propertiesDict) {
		this.propertiesDict = propertiesDict;
	}

	/**
	 * Metoda umożliwiająca tworzenie miasta.
     */
	public void create() {
        try {
        	mosEndpoint.addCity(city);
            addOperationMessage(false, "citycreated");
            this.city.setName("");
        } catch (ApplicationException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(CreateCityPageBean.class.getName()).log(Level.SEVERE, null, ex);
            addMessage(true, ex.getMessage());
        }
    }

	/**
	 * Dodanie komunikat o wyniku operacji
	 * @param isError Sprawdza, czy wystąpił błąd
	 * @param detail Nazwa property, której wartość wyświetlana jest użytkownikowi
	 */
	private void addOperationMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, propertiesDict.getString(detail));
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

	/**
	 * Dodanie komunikatu w przypadku niepowodzenia operacji
	 * @param isError Sprawdza, czy wystąpił błąd
	 * @param detail Wiadomość ze złapanego wyjątku
	 */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

}
