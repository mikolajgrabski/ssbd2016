/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mos.facades;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.University;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.UniversityException;

import javax.ejb.Local;
import java.util.List;


/**
 * @author Patryk
 */
@Local
public interface UniversityFacadeLocal {

    void create(University university) throws ApplicationException;

    void edit(University university) throws ApplicationException;

    void remove(University university);

    University find(Object id) throws ApplicationException;

    List<University> findAll() throws ApplicationException;

    List<University> findRange(int[] range);

    int count();

    University findByName(String name) throws UniversityException;

    /**
     * Przeprowadza wyszukiwanie uczelni po id
     * @param id id uczelni
     * @return odnaleziona uczelnia
     * @throws UniversityException
     */
    University findById(Object id) throws UniversityException;
    
}
