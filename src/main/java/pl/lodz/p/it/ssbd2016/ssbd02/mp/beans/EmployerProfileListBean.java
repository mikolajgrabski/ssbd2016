package pl.lodz.p.it.ssbd2016.ssbd02.mp.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.mp.endpoints.MPEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Bean do wyświetlania listy ogłoszeń
 * Created by ljaros on 6/8/16.
 */
@Named("employerProfileListBean")
@ViewScoped
public class EmployerProfileListBean implements Serializable {
    @EJB
    private MPEndpointLocal mpEndpoint;
    private DataModel<Employer> employerDataModel;
    private String errorMessage;
    private boolean error = false;
    private Text propertiesDict;
    private boolean moderator;

    public boolean isModerator() {
        return moderator;
    }

    public void setModerator(boolean moderator) {
        this.moderator = moderator;
    }

    public EmployerProfileListBean() {
        this.propertiesDict = new Text();
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    /**
     * Inicjalizuje pola klasy, pobiera listę wszystkich porfili pracodawcy
     */
    @PostConstruct
    public void initModel() {
        try {
            employerDataModel = new ListDataModel<Employer>(mpEndpoint.getAllEmployers());
            moderator = mpEndpoint.isModerator();
        } catch (ApplicationException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(EmployerProfileListBean.class.getName()).log(Level.SEVERE, null, ex);
            addMessage(true, ex.getMessage());
        }
    }

    public DataModel<Employer> getEmployerDataModel() {
        return employerDataModel;
    }

    public void setEmployerDataModel(DataModel<Employer> employerDataModel) {
        this.employerDataModel = employerDataModel;
    }

    /**
     * Zapisuje w pamięci flash konto do wyświetlenia i przenosi na na nową podstronę.
     * @return podstrona, na którą ma być przeniesienie
     */
    public String prepareProfileView() {
        Employer employerToShow = employerDataModel.getRowData();
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("employerToShow", employerToShow);
        return "employer-profile";
    }

    /**
     * Akceptuje profil pracodawcy
     */
    public void acceptProfile() {
        try {
            Employer employerToAccept = employerDataModel.getRowData();
            mpEndpoint.acceptEmployer(employerToAccept);
            addOperationMessage(false, "employerconfirmed");
        } catch (ApplicationException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(EmployerProfileListBean.class.getName()).log(Level.SEVERE, null, ex);
            addMessage(true, ex.getMessage());
        }
    }

    /**
     * Dodanie komunikat o wyniku operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Nazwa property, której wartość wyświetlana jest użytkownikowi
     */
    private void addOperationMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, propertiesDict.getString(detail));
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Dodanie komunikatu w przypadku niepowodzenia operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Wiadomość ze złapanego wyjątku
     */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}

