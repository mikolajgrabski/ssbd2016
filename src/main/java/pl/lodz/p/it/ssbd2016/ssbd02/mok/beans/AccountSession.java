package pl.lodz.p.it.ssbd2016.ssbd02.mok.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Account;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.EmailException;
import pl.lodz.p.it.ssbd2016.ssbd02.mok.endpoints.MOKEndpointLocal;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * Ziarno sesji użytkownika.
 * @author mgrabski
 */

@Named
@SessionScoped
public class AccountSession implements Serializable {

    @EJB
    private MOKEndpointLocal mokEndpoint;

    private Account accountToEdit;
    private Account currentAccount;

    public AccountSession() {
    }

    /**
     * Metoda pobiera konto do edycji.
     * @param account
     * @throws ClassNotFoundException
     * @throws IOException
     * @throws AccountException
     */
    private void receiveAccountToEdit(Account account)
            throws ClassNotFoundException, IOException, AccountException {
        this.accountToEdit = mokEndpoint.getCurrentAccount(account);
    }

    /**
     * Metoda pobierająca konto zalogowanego użytkownika przy pomocy funkcji
     * zawartej w endpoincie.
     * @return currentAccount Zwraca konto.
     */
    public Account getCurrentAccount() {
        try {
            currentAccount = mokEndpoint.getMyAccount();

        } catch (AccountException e) {
            e.printStackTrace();
        }

        return currentAccount;
    }

    /**
     * Metoda pobierająca i zwracająca konto użytkownika o podanym loginie przy
     * pomocy metody zawartej w endpoincie.
     * @param login 
     * @return Zwraca obiekt typu Account.
     * @throws AccountException
     */
    public Account getAccountByLogin(String login) throws AccountException {
        return mokEndpoint.getAccountByLogin(login);
    }

    public void setCurrentAccount(Account account) {
        this.currentAccount = account;
    }

    /**
     * Metoda pobierająca konto do edycji.
     * @param account
     * @return accountToEdit Zwraca obiekt typu Account.
     * @throws AccountException
     */
    public Account getAccountToEdit(Account account) throws AccountException {
        try {
            receiveAccountToEdit(account);
        } catch (ClassNotFoundException | IOException e) {
            this.accountToEdit = new Account();
            e.printStackTrace();
        }
        return accountToEdit;
    }

    public Account getLocalAccountToEdit(){
        return accountToEdit;
    }

    public void setAccountToEdit(Account accountToEdit) {
        this.accountToEdit = accountToEdit;
    }

    /**
     * Metoda umożliwiająca dokonanie rejestracji użytkownika przy użyciu
     * funkcji zawartych w endpoincie.
     * @param account Tworzone konto użytkownika
     * @throws AccountException
     */
    public void registerAccount(Account account) throws AccountException, EmailException {
        account.setActive(false);
        account.setProfileType(account.getProfileType());

        mokEndpoint.registerAccount(account);
    }

    /**
     * Metoda umożliwiająca dokonanie utworzenia konta przez administratora
     * przy użyciu metody zawartej w endpoincie.
     * @param account Tworzone konto
     * @throws AccountException
     */
    public void createAccount(Account account) throws AccountException {
        mokEndpoint.createAccount(account);
    }
    
    /**
     * Metoda umożiwiająca aktywację konta użytkownika przy użyciu metody
     * zawartej w endpoincie.
     * @param account Konto do aktywacji
     * @throws AccountException
     */
    public void activateAccount(Account account) throws AccountException, EmailException {
        mokEndpoint.activateAccount(account);
    }

    /**
     * Metoda umożiwiająca deaktywację konta użytkownika przy użyciu metody
     * zawartej w endpoincie.
     * @param account Konto do deaktywacji
     * @throws AccountException
     */
    public void deactivateAccount(Account account) throws AccountException, EmailException {
        mokEndpoint.deactivateAccount(account);
    }

    /**
     * Metoda umożliwiająca zmianę własnego hasła przy pomocy metody zawartej
     * w endpoincie.
     * @param oldPassword Dotychczasowe hasło
     * @param newPassword Nowe hasło
     * @throws AccountException
     */
    public void changePassword(String oldPassword, String newPassword) throws AccountException {
        mokEndpoint.changePassword(oldPassword, newPassword);
    }

    /**
     * Metoda umożliwiająca zmianę hasła dowolnego użytkownika przy pomocy metody
     * zawartej w endpoincie.
     * @param account Wybrane konto
     * @param newPassword Nowe hasło
     * @throws AccountException
     */
    public void changeUserPassword(Account account, String newPassword) throws AccountException{
        mokEndpoint.changeUserPassword(account, newPassword);
    }
    
    /**
     * Metoda umożliwiająca edycję dowolnego konta użytkownika przy pomocy metody
     * zawartej w endpoincie
     * @param oldAccount Dotychczasowy stan konta
     * @param newAccount Nowy stan konta
     * @throws AccountException
     */
    public void editAccount(Account oldAccount, Account newAccount) throws AccountException {
        mokEndpoint.editAccount(oldAccount, newAccount);
    }

    /**
     * Metoda umożliwiająca pobranie i zwrócenie listy wszystkich użytkowników przy pomocy
     * metody zawartej w endpoincie.
     * @return zwraca listę obiektów typu Account
     * @throws AccountException
     */
    public List<Account> getAllAccounts() throws AccountException {
        return mokEndpoint.getAllAccounts();
    }

    /**
     * Metoda umożliwiająca zmianę listy poziomów dostępu przy pomocy metody zawartej w
     * endpoincie.
     * @param relatedAccount Wybrane konto
     * @throws AccountException
     */
    public void changeAccessLevels(Account relatedAccount) throws AccountException{
        mokEndpoint.changeAccessLevels(relatedAccount);
    }

    /**
     * Metoda umożliwiająca wylogowanie użytkownika.
     * @throws IOException
     */
    public void logout() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

        try {
            request.logout();
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }

    public void editLoginStats(Account account) throws AccountException, EmailException {
        mokEndpoint.editLoginStats(account);
    }
}
