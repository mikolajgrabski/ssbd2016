package pl.lodz.p.it.ssbd2016.ssbd02.mp.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Category;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployerLocation;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.mok.endpoints.MOKEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.mp.endpoints.MPEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Bean do edycji konta pracodawcy
 * @author Kacper Dobrzański
 */
@Named("editEmployerBean")
@ViewScoped
public class EditEmployerBean implements Serializable {
    @EJB
    private MPEndpointLocal mpEndpoint;
    @EJB
    private MOKEndpointLocal mokEndpoint;

    private Text propertiesDict = new Text();

    private String errorMessage;
    private boolean error = false;

    private Employer employer;
    private Category newCategory;
    private EmployerLocation newLocation;

    private List<Category> categoryList;

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public Category getNewCategory() {
        return newCategory;
    }

    public void setNewCategory(Category newCategory) {
        this.newCategory = newCategory;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public EmployerLocation getNewLocation() {
        return newLocation;
    }

    public void setNewLocation(EmployerLocation newLocation) {
        this.newLocation = newLocation;
    }

    /**
     * Inicjalizuje zmienne
     */
    @PostConstruct
    public void initModel() {
        try {
            this.employer = mpEndpoint.getEmployer(mokEndpoint.getMyAccount().getEmployer());
            if (this.categoryList == null) {
                this.categoryList = mpEndpoint.getAllCategories();
            }
            this.newLocation = new EmployerLocation(this.employer);
        } catch (ApplicationException | IOException | ClassNotFoundException e) {
            setError(true);
            setErrorMessage(e.getMessage());
            Logger.getLogger(EditEmployeeBean.class.getName()).log(Level.SEVERE, null, e);
            addMessage(true, e.getMessage());
        }
    }

    /**
     * Dodaje kategorie
     */
    public void addCategory() {
        this.employer.getCategoryList().add(this.newCategory);
        this.newCategory = new Category();
    }

    /**
     * Dodaje lokalizacje
     */
    public void addLocation() {
        this.employer.getEmployerLocationList().add(this.newLocation);
        this.newLocation = new EmployerLocation(this.employer);
    }

    /**
     * Usuwa kategorię
     * @param category kategoria do usunięcia
     */
    public void deleteCategory(Category category) {
        this.employer.getCategoryList().remove(category);
    }

    /**
     * Usuwa lokalizację
     * @param location lokalizacja do usunięcia
     */
    public void deleteEmployerLocation(EmployerLocation location) {
        this.employer.getEmployerLocationList().remove(location);
    }

    /**
     * Edytuje profil pracodawcy
     */
    public void editProfile() {
        try {
            mpEndpoint.editEmployer(employer);
            addOperationMessage(false, "profileedited");
        } catch (ApplicationException e) {
            setError(true);
            setErrorMessage(e.getMessage());
            Logger.getLogger(EditEmployeeBean.class.getName()).log(Level.SEVERE, null, e);
            addMessage(true, e.getMessage());
        }
        initModel();
    }

    /**
     * Dodanie komunikat o wyniku operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Nazwa property, której wartość wyświetlana jest użytkownikowi
     */
    private void addOperationMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, propertiesDict.getString(detail));
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Dodanie komunikatu w przypadku niepowodzenia operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Wiadomość ze złapanego wyjątku
     */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

}
