/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mp.facades;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AbstractFacade;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.City;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Fasada do zarządzania miastami
 * @author Patryk
 */
@Stateless(name = "CityFacadeMP")
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class CityFacade extends AbstractFacade<City> implements CityFacadeLocal {

    @PersistenceContext(unitName = "ssbd02mpPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CityFacade() {
        super(City.class);
    }

    @Override
    @RolesAllowed({"editEmployee", "editEmployer"})
    public void create(City city) {
        em.persist(city);
        em.flush();
    }

    @Override
    @RolesAllowed({"editEmployee", "editEmployer"})
    public City findByName(String name) {
        try {
            Query q = em.createNamedQuery("City.findByName");
            q.setParameter("name", name);
            return (City) q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

}
