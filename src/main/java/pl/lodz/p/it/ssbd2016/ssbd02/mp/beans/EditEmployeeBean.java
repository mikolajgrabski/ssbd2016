package pl.lodz.p.it.ssbd2016.ssbd02.mp.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.*;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.mok.endpoints.MOKEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.mos.endpoints.MOSEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.mp.endpoints.MPEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Kacper Dobrzański
 */

/**
 * Bean zajmujący się edycją konta pracownika
 */
@Named("editEmployeeBean")
@ViewScoped
public class EditEmployeeBean implements Serializable {
    @EJB
    private MPEndpointLocal mpEndpoint;
    @EJB
    private MOSEndpointLocal mosEndpoint;
    @EJB
    private MOKEndpointLocal mokEndpoint;
    private Text propertiesDict = new Text();

    private EmployeeLanguage newEmployeeLanguage;
    private EmployeeEducation newEmployeeEducation;
    private EmployeeJobHistory newJobHistory;
    private String newCityName;

    private String errorMessage;
    private boolean error = false;

    private Employee employee;
    private List<University> universities;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public EmployeeJobHistory getNewJobHistory() {
        return newJobHistory;
    }

    public String getNewCityName() {
        return newCityName;
    }

    public void setNewCityName(String newCityName) {
        this.newCityName = newCityName;
    }

    public void setNewJobHistory(EmployeeJobHistory newJobHistory) {
        this.newJobHistory = newJobHistory;
    }

    public void setNewEmployeeLanguage(EmployeeLanguage newEmployeeLanguage) {
        this.newEmployeeLanguage = newEmployeeLanguage;
    }

    public EmployeeLanguage getNewEmployeeLanguage() {
        return newEmployeeLanguage;
    }

    public EmployeeEducation getNewEmployeeEducation() {
        return newEmployeeEducation;
    }

    public void setNewEmployeeEducation(EmployeeEducation newEmployeeEducation) {
        this.newEmployeeEducation = newEmployeeEducation;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<University> getUniversities() {
        return universities;
    }

    public void setUniversities(List<University> universities) {
        this.universities = universities;
    }

    /**
     * Inicjalizuje zmienne
     */
    @PostConstruct
    public void initModel() {
        try {
            this.employee = mpEndpoint.getEmployee(mokEndpoint.getMyAccount().getEmployee());
            this.newEmployeeLanguage = new EmployeeLanguage(this.employee);
            this.newEmployeeEducation = new EmployeeEducation(this.employee);
            this.newJobHistory = new EmployeeJobHistory(this.employee);
            this.newCityName = employee.getCityId().getName();
            if (universities == null) {
                universities = mosEndpoint.getUniversities();
            }
        } catch (ApplicationException | IOException | ClassNotFoundException e) {
            setError(true);
            setErrorMessage(e.getMessage());
            Logger.getLogger(EditEmployeeBean.class.getName()).log(Level.SEVERE, null, e);
            addMessage(true, e.getMessage());
        }
    }

    /**
     * Edytuje profil pracownika
     */
    public void editProfile() {

        try {
            mpEndpoint.editEmployee(employee, newCityName);
            addOperationMessage(false, "profileedited");
            initModel();
        } catch (ApplicationException e) {
            setError(true);
            setErrorMessage(e.getMessage());
            Logger.getLogger(EditEmployeeBean.class.getName()).log(Level.SEVERE, null, e);
            addMessage(true, e.getMessage());
        }
    }

    /**
     * Usuwa język z listy języków pracownika
     * @param employeeLanguage język do usunięcia
     */
    public void deleteLanguage(EmployeeLanguage employeeLanguage) {
        this.employee.getEmployeeLanguageList().remove(employeeLanguage);
    }

    /**
     * Dodaje język do listy języków pracownika
     */
    public void addLanguage() {
        this.employee.getEmployeeLanguageList().add(this.newEmployeeLanguage);
        this.newEmployeeLanguage = new EmployeeLanguage(this.employee);
    }

    /**
     * Usuwa informacje o wykształceniu
     * @param employeeEducation wykształcenie
     */
    public void deleteEducation(EmployeeEducation employeeEducation) {
        this.employee.getEmployeeEducationList().remove(employeeEducation);
    }

    /**
     * Dodaje informacje o wykształceniu
     */
    public void addEducation() {
        try {
            if (this.newEmployeeEducation.getUniversityId() == null) {
                setError(true);
                addOperationMessage(true, "selectuniversity");
            } else if (newEmployeeEducation.getDateTo() != null && newEmployeeEducation.getDateFrom().after
                    (newEmployeeEducation.getDateTo())) {
                setError(true);
                addOperationMessage(true, "incorrectdates");
            } else {
                this.employee.getEmployeeEducationList().add(this.newEmployeeEducation);
            }
        } finally {
            this.newEmployeeEducation = new EmployeeEducation(this.employee);
        }
    }

    /**
     * Usuwa historę zatrudnienia
     * @param employeeJob historia zatrudnienia
     */
    public void deleteJobHistory(EmployeeJobHistory employeeJob) {
        this.employee.getEmployeeJobHistoryList().remove(employeeJob);
    }

    /**
     * Dodaje historię zatrudnienia
     */
    public void addJobHistory() {
        try {
            if (newJobHistory.getDateTo() != null && newJobHistory.getDateTo().before(newJobHistory.getDateFrom())) {
                setError(true);
                addOperationMessage(true, "incorrectdates");
            } else {
                this.employee.getEmployeeJobHistoryList().add(this.newJobHistory);
            }
        } finally {
            this.newJobHistory = new EmployeeJobHistory(this.employee);
        }
    }

    /**
     * Dodanie komunikat o wyniku operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Nazwa property, której wartość wyświetlana jest użytkownikowi
     */
    private void addOperationMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, propertiesDict.getString(detail));
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Dodanie komunikatu w przypadku niepowodzenia operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Wiadomość ze złapanego wyjątku
     */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
