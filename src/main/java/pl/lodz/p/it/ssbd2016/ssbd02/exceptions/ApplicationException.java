/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.exceptions;

/**
 *
 * @author piotr
 */
@javax.ejb.ApplicationException(rollback = true)
abstract public class ApplicationException extends Exception {

    public ApplicationException() {}
    
    public ApplicationException(String message) {
        super(message);
    }
    
    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }
}
