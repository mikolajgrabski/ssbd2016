package pl.lodz.p.it.ssbd2016.ssbd02.mos.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.City;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.mos.endpoints.MOSEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named("listCityBean")
@RequestScoped
public class ListCityBean {

    @EJB
    private MOSEndpointLocal mosEndpoint;
    private DataModel<City> cityDataModel;
    private City currentCity;
    private Text propertiesDict;
    private boolean error = false;
    private String errorMessage;

    @PostConstruct
    public void initModel() {
        try {
            cityDataModel = new ListDataModel<>(mosEndpoint.getCities());
            propertiesDict = new Text();
        } catch (ApplicationException e) {
            e.printStackTrace();
            setError(true);
            setErrorMessage(e.getMessage());
            addMessage(true, e.getMessage());
            Logger.getLogger(ListCityBean.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public DataModel<City> getCityDataModel() {
        return cityDataModel;
    }

    public void setCityDataModel(DataModel<City> cityDataModel) {
        this.cityDataModel = cityDataModel;
    }

    private void setError(boolean error) {
        this.error = error;
    }

    public boolean isError() {
        return error;
    }

    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Dodanie komunikatu w przypadku niepowodzenia operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Wiadomość ze złapanego wyjątku
     */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public String initEdit() {
        currentCity = cityDataModel.getRowData();
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("cityToEdit", currentCity);
        return "edit-city";
    }

}
