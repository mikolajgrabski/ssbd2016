package pl.lodz.p.it.ssbd2016.ssbd02.mop.facades;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AbstractFacade;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployerRating;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.RatingException;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.util.List;

/**
 * Fasada do zarządzania ocenami pracodawcy
 * @author Patryk
 */
@Stateless(name = "EmployerRatingFacadeMOP")
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class EmployerRatingFacade extends AbstractFacade<EmployerRating> implements EmployerRatingFacadeLocal {

    @PersistenceContext(unitName = "ssbd02mopPU")
    private EntityManager em;
    private List employerRatings;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmployerRatingFacade() {
        super(EmployerRating.class);
    }

    @Override
    @RolesAllowed("getNotAcceptedOpinions")
    public List<EmployerRating> findByAccepted(boolean accepted) throws RatingException {
        try {
            employerRatings = em.createNamedQuery("EmployerRating.findByAccepted")
                    .setParameter("accepted", accepted).getResultList();
        } catch (NoResultException ex) {
            throw RatingException.createNoRatingsInDbException();
        }
        return employerRatings;
    }

    @Override
    @RolesAllowed("acceptOpinion")
    public List<EmployerRating> findAcceptedByEmployer(Employer employer) throws RatingException {
        try {
            employerRatings = em.createNamedQuery("EmployerRating.findAcceptedByEmployer")
                    .setParameter("employerId", employer).getResultList();
        } catch (NoResultException ex) {
            throw RatingException.createNoRatingsInDbException();
        }
        return employerRatings;
    }

    @Override
    @PermitAll
    public void create(EmployerRating employerRating) throws ApplicationException {
        try {
            em.persist(employerRating);
            em.flush();
        } catch (PersistenceException ex) {
            if (ex.getMessage().contains("employer_rating_unique"))
                throw RatingException.createNotUniqueRatingException(ex, employerRating);
        }
    }
}
