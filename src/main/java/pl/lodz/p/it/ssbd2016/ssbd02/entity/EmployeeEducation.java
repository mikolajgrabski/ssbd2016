/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Patryk
 */
@Entity
@Table(name = "employee_education")
@NamedQueries({
    @NamedQuery(name = "EmployeeEducation.findAll", query = "SELECT e FROM EmployeeEducation e"),
    @NamedQuery(name = "EmployeeEducation.findById", query = "SELECT e FROM EmployeeEducation e WHERE e.id = :id"),
    @NamedQuery(name = "EmployeeEducation.findByMajor", query = "SELECT e FROM EmployeeEducation e WHERE e.major = :major"),
    @NamedQuery(name = "EmployeeEducation.findByDateFrom", query = "SELECT e FROM EmployeeEducation e WHERE e.dateFrom = :dateFrom"),
    @NamedQuery(name = "EmployeeEducation.findByDateTo", query = "SELECT e FROM EmployeeEducation e WHERE e.dateTo = :dateTo")})
public class EmployeeEducation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "major")
    private String major;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_from")
    @Temporal(TemporalType.DATE)
    private Date dateFrom;
    @Column(name = "date_to")
    @Temporal(TemporalType.DATE)
    private Date dateTo;
    @JoinColumn(name = "employee_id", referencedColumnName = "id", updatable = false)
    @ManyToOne(optional = false)
    private Employee employeeId;
    @JoinColumn(name = "university_id", referencedColumnName = "id", updatable = false)
    @ManyToOne
    private University universityId;

    public EmployeeEducation() {
    }

    public EmployeeEducation(Integer id) {
        this.id = id;
    }

    public EmployeeEducation(Employee employee) {
        this.employeeId = employee;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Employee getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Employee employeeId) {
        this.employeeId = employeeId;
    }

    public University getUniversityId() {
        return universityId;
    }

    public void setUniversityId(University universityId) {
        this.universityId = universityId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmployeeEducation)) {
            return false;
        }
        EmployeeEducation other = (EmployeeEducation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployeeEducation[ id=" + id + " ]";
    }
    
}
