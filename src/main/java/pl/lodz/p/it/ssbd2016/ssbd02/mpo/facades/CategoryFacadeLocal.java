/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mpo.facades;

import java.util.List;
import javax.ejb.Local;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Category;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

/**
 *
 * @author Patryk
 */
@Local
public interface CategoryFacadeLocal {

    void create(Category category) throws ApplicationException;

    void edit(Category category) throws ApplicationException;

    void remove(Category category);

    Category find(Object id) throws ApplicationException;

    List<Category> findAll() throws ApplicationException;

    List<Category> findRange(int[] range);

    int count();
    
}
