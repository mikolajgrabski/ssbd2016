package pl.lodz.p.it.ssbd2016.ssbd02.mok.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Account;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Ziarno widoku tworzenia konta użytkownika przez administratora.
 * @author mgrabski
 */

@Named("createPageBean")
@RequestScoped
public class CreatePageBean {

	/**
	 * Domyślny konstruktor bezparmetrowy, w którym incjalizowane są zmienne
	 * propertiesDict oraz account.
	 */
    public CreatePageBean() {
        propertiesDict = new Text();
        account = new Account(true);
    }

    @Inject
    private AccountSession accountSession;

    private Account account;
    private String errorMessage;
    private boolean error = false;
    private Text propertiesDict;

    public String getErrorMessage() {
        return errorMessage;
    }

    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isError() {
        return error;
    }

    private void setError(boolean error) {
        this.error = error;
    }

    public Account getAccount() {
        return account;
    }

    /**
     * Metoda umożliwiająca stworzenie konta użytkownika.
     * @throws AccountException
     */
    public void create() throws AccountException {
        try {
            accountSession.createAccount(account);
            addOperationMessage(false, "accountcreated");
            this.account.setLogin("");
            this.account.setEmail("");
        } catch (ApplicationException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(CreatePageBean.class.getName()).log(Level.SEVERE, null, ex);
            addMessage(true, ex.getMessage());
        }
    }

    /**
     * Metoda odpowiedzialna za zresetowanie stanu listy wybranych poziomów dostępu.
     */
    public void resetAccessLevels(){
        account.setAllAccessLevels();
    }

	/**
	 * Dodanie komunikatu o wyniku operacji
	 * @param isError Sprawdza, czy wystąpił błąd
	 * @param detail Nazwa property, której wartość wyświetlana jest użytkownikowi
	 */
    private void addOperationMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, propertiesDict.getString(detail));
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

	/**
	 * Dodanie komunikatu w przypadku niepowodzenia operacji
	 * @param isError Sprawdza, czy wystąpił błąd
	 * @param detail Wiadomość ze złapanego wyjątku
	 */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

}
