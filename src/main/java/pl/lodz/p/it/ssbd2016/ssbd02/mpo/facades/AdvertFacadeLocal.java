package pl.lodz.p.it.ssbd2016.ssbd02.mpo.facades;

import java.util.Date;
import java.util.List;
import javax.ejb.Local;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Advert;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AdvertException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

@Local
public interface AdvertFacadeLocal {

    /**
     * Szuka konkretnego ogłoszenia.
     * @param id Id ogłoszenia
     * @return Ogłoszenie
     * @throws ApplicationException
     */
    Advert find(Object id) throws ApplicationException;

    List<Advert> findRange(int[] range);

    /**
     * Zwraca listę ogłoszeń
     * @param phrase fraza,do wyszukiwania ogłoszeń
     * @param cityName nazw miasta
     * @return lista ogłoszeń
     * @throws AdvertException
     */
    List<Advert> searchForAdvert(String phrase, String cityName) throws AdvertException;

    /**
     * Wyszukuje ogłoszenia dla danego miasta
     * @param cityName nazwa miasta
     * @return lista ogłoszeń
     * @throws AdvertException
     */
    List<Advert> searchForAdvert(String cityName) throws AdvertException;

    /**
     * Wyszukiwanie wsyztkich ogłoszeń
     * @return Liste ogłoszeń
     * @throws AdvertException
     */
    List<Advert> findAll() throws AdvertException;

    /**
     * Szuka konkretnego ogłoszenia.
     * @param id Id ogłoszenia
     * @return Ogłoszenie
     * @throws AdvertException
     */
    Advert findById(Integer id) throws AdvertException;

    /**
     * Znajduje wszystkie ogłoszenia z datą późniejszą niż podana
     * @param date data wygaśnięcia
     * @return
     * @throws AdvertException
     */
    List<Advert> findByExpiryDate(Date date) throws AdvertException;

    /**
     * Zwraca listę ogłoszeń zawierających podaną frazę
     * @param phrase
     * @param cityName
     * @return lista ogłoszeń
     */
    List<Advert> findByQuery(String phrase, String cityName);

    /**
     * Edytuje ogłoszenie
     * @param advert Ogłoszenie
     * @throws ApplicationException
     */
    void edit(Advert advert) throws ApplicationException;
}
