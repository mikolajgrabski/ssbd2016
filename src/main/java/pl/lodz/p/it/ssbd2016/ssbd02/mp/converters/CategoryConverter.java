package pl.lodz.p.it.ssbd2016.ssbd02.mp.converters;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Category;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.mp.endpoints.MPEndpointLocal;

import javax.ejb.EJB;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * @author Kacper Dobrzański
 */

/**
 * Zajmuje się konwersją obiektów typu String na obiekt typu Category
 */
@FacesConverter("categoryConverter")
public class CategoryConverter implements Converter {
    @EJB
    private MPEndpointLocal mpEndpoint;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        try {
            return mpEndpoint.findCategoryByName(s);
        } catch (ApplicationException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        return ((Category) o).getName();
    }
}
