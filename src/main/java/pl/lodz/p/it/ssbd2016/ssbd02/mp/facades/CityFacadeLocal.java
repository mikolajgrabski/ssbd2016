/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mp.facades;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.City;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

import javax.ejb.Local;
import java.util.List;

/**
 * @author Patryk
 */
@Local
public interface CityFacadeLocal {
    /**
     * Tworzy nowe miasto
     * @param city
     */
    void create(City city);

    void edit(City city) throws ApplicationException;

    void remove(City city);

    City find(Object id) throws ApplicationException;

    List<City> findAll() throws ApplicationException;

    List<City> findRange(int[] range);

    int count();

    /**
     * Zwraca miasto o podanej nazwie
     * @param name nazwa miasta
     * @return miasto o podanej nazwie
     */
    City findByName(String name);
}
