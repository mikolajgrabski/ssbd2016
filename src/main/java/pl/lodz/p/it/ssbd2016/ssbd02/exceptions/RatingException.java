package pl.lodz.p.it.ssbd2016.ssbd02.exceptions;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployerRating;

import javax.faces.context.FacesContext;
import java.util.ResourceBundle;

/**
 * Created by piotr on 16.05.16.
 */
public class RatingException extends ApplicationException {
    public static final String KEY_PERSISTENCE_EXCEPTION_NOT_UNIQUE_RATING = "RATING.EXCEPTION.PERSISTENCE_EXCEPTION_NOT_UNIQUE_RATING";
    public static final String KEY_RATING_CONCURENT_MODIFICATION = "RATING.EXCEPTION.RATING_CONCURENT_MODIFICATION";
    public static final String KEY_RATING_CHANGED_AFTER_READING = "RATING.EXCEPTION.RATING_CHANGED_AFTER_READING";
    public static final String KEY_NO_RATINGS_IN_DB = "RATING.EXCEPTION.NO_RATINGS_IN_DB";
    public static final String KEY_EMPLOYER_NOT_EXISTS = "RATING.EXCEPTION.EMPLOYER_NOT_EXISTS";
    private EmployerRating employerRating;

    public RatingException() {}
    public RatingException(String message) {
        super(message);
    }
    public RatingException(String message, Throwable cause) {
        super(message, cause);
    }

   public static RatingException createConcurentModificationException(Throwable cause, EmployerRating employerRating) {
        return createException(cause, employerRating, KEY_RATING_CONCURENT_MODIFICATION);
    }

    public static RatingException createChangedAfterReadingException(Throwable cause, EmployerRating employerRating) {
        return createException(cause, employerRating, KEY_RATING_CHANGED_AFTER_READING);
    }

    public static RatingException createNotUniqueRatingException(Throwable cause, EmployerRating employerRating) {
        return createException(cause, employerRating, KEY_PERSISTENCE_EXCEPTION_NOT_UNIQUE_RATING);
    }

    public static RatingException createEmployerNotExistsException(Throwable cause, EmployerRating employerRating) {
        return createException(cause, employerRating, KEY_EMPLOYER_NOT_EXISTS);
    }

    public static RatingException createNoRatingsInDbException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_NO_RATINGS_IN_DB);

        RatingException employeeException = new RatingException(message);
        return employeeException;
    }

    private static RatingException createException(Throwable cause, EmployerRating employerRating, String msg) {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(msg);

        RatingException employeeException = new RatingException(message, cause);
        employeeException.setRating(employerRating);
        return employeeException;
    }

    public void setRating(EmployerRating employerRating) {
        this.employerRating = employerRating;
    }
}
