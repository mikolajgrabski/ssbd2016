/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mteo.facades;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AbstractFacade;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.EmployerException;

/**
 *
 * @author Patryk
 */

/**
 * Fasada do operacji na bazie danych dotyczących konta pracodawcy.
 */
@Stateless(name = "EmployerFacadeMTEO")
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class EmployerFacade extends AbstractFacade<Employer> implements EmployerFacadeLocal {

    @PersistenceContext(unitName = "ssbd02mteoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmployerFacade() {
        super(Employer.class);
    }

    @Override
    @RolesAllowed("findByAccount")
    public Employer findByAccout(String login) throws ApplicationException {
        Query q = em.createNamedQuery("Employer.findByAccountLogin");
        q.setParameter("login", login);
        try {
            return (Employer) q.getSingleResult();
        } catch(NoResultException ex) {
            throw EmployerException.createNoEmployerInDbException();
        } catch (Exception e) {
            throw EmployerException.createUnknownException();
        }
    }

}
