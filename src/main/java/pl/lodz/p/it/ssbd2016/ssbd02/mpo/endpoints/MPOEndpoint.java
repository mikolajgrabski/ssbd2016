package pl.lodz.p.it.ssbd2016.ssbd02.mpo.endpoints;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Advert;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.AdvertResponse;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.City;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AdvertException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AdvertResponseException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.mok.beans.AccountSession;
import pl.lodz.p.it.ssbd2016.ssbd02.mok.endpoints.MOKEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.mpo.facades.AdvertFacadeLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.mpo.facades.AdvertResponseFacadeLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.mpo.facades.CityFacadeLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.utils.CloneUtils;

import javax.annotation.Resource;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.*;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Patryk Gutenplan
 * Endpoint do przeglądania ogłoszeń
 */
@Stateful
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class MPOEndpoint implements MPOEndpointLocal, SessionSynchronization {
    private static final Logger logger = Logger.getLogger(MPOEndpoint.class.getName());

    @Resource
    private SessionContext sctx;
    @EJB
    private AdvertFacadeLocal advertFacade;
    @EJB
    private AdvertResponseFacadeLocal advertResponseFacade;
    @EJB
    private CityFacadeLocal cityFacade;
    private MOKEndpointLocal mokEndpoint;
    @Inject
    private AccountSession accountSession;

    private Advert currentAdvert;
    private List<AdvertResponse> currentAdvertResponseList;
    private long transactionId;
    private AdvertResponse advertResponse;

    @Override
    @PermitAll
    public void setCurrentAdvert(Integer id) throws AdvertException, IOException, ClassNotFoundException {
        this.currentAdvert = advertFacade.findById(id);
    }

    @Override
    @PermitAll
    public List<Advert> getAdverts() throws AdvertException {
        return advertFacade.findByExpiryDate(new Date());
    }

    @Override
    @RolesAllowed("searchForAdvert")
    public List<Advert> searchForAdvert(String phrase, String cityName) throws AdvertException {
        if(phrase.isEmpty()) {
            return advertFacade.searchForAdvert(cityName);
        }
        // %s allow to perform LIKE query
        phrase = "%" + phrase + "%";
        cityName = "%" + phrase + "%";
        return advertFacade.searchForAdvert(phrase, cityName);
    }

    @Override
    @RolesAllowed("getAdvert")
    public Advert getAdvert(Integer id) throws AdvertException, IOException, ClassNotFoundException {
        return advertFacade.findById(id);
    }

    @Override
    @RolesAllowed("sendResponse")
    public void sendResponse(Advert currentAdvert) throws ApplicationException {

        if (new Date().after(currentAdvert.getExpiryDate()))
            throw AdvertResponseException.createAdvertResponseDateException();
        if (currentAdvert.getResponsesLeft() < 0)
            throw AdvertResponseException.createNoResponseLeftAdvertException();

        advertResponse = new AdvertResponse();
        advertResponse.setEmployeeId(mokEndpoint.getAccountByLogin(accountSession.getCurrentAccount().getLogin()).getEmployee());
        advertResponse.setResponseTime(new Date());
        advertResponse.setAdvertId(currentAdvert);
        this.advertResponseFacade.create(advertResponse);
        short resLeft = currentAdvert.getResponsesLeft();
        resLeft--;
        currentAdvert.setResponsesLeft(resLeft);

        try {
            advertFacade.edit(currentAdvert);
        } catch (ApplicationException ex) {
            if (currentAdvert.getResponsesLeft() == 0) throw new AdvertResponseException();
        }
    }

    @Override
    @RolesAllowed("getResponses")
    public List<AdvertResponse> getResponses(Integer advertId)
            throws ApplicationException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException,
            InvocationTargetException, InstantiationException, IOException {
        currentAdvert = advertFacade.findById(advertId);
        currentAdvertResponseList = (List<AdvertResponse>) CloneUtils.deepCloneList(currentAdvert.getAdvertResponseList());
        return currentAdvertResponseList;
    }

    @Override
    @RolesAllowed("searchForAdvert")
    public List<City> getCities() throws ApplicationException {
        return cityFacade.findAll();
    }

    @Override
    @RolesAllowed("getResponse")
    public AdvertResponse getResponse(Integer responseId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void afterBegin() throws EJBException, RemoteException {
        transactionId = System.currentTimeMillis();
        logger.log(Level.SEVERE, "Transaction (ID:" + transactionId + ") has " +
                "started by user with ID: " + sctx.getCallerPrincipal().getName() + ".");
    }

    @Override
    public void beforeCompletion() throws EJBException, RemoteException {
        logger.log(Level.SEVERE, "Transaction (ID:" + transactionId + ") " +
                "started by user with ID: " + sctx.getCallerPrincipal().getName() +
                " before completion.");
    }

    @Override
    public void afterCompletion(boolean committed) throws EJBException, RemoteException {
        logger.log(Level.SEVERE, "Transaction (ID:" + transactionId + ") " +
                "started by user with ID: " + sctx.getCallerPrincipal().getName() +
                " was ended because of: " + (committed ? "COMMIT." : "ROLLBACK."));
    }

    @Override
    @RolesAllowed("editAdvert")
    public void editAdvert(Advert advert) throws ApplicationException {
        currentAdvert = advert;
        advertFacade.edit(currentAdvert);

        currentAdvert = null;
    }

    @Override
    public Advert getCurrentAdvert() {
        return currentAdvert;
    }
}
