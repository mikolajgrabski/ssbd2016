/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mp.facades;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AbstractFacade;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.University;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.EmployerException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.UniversityException;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Fasada do zarządzania uniwersytami
 * @author Patryk
 */
@Stateless(name = "UniversityFacadeMP")
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class UniversityFacade extends AbstractFacade<University> implements UniversityFacadeLocal {

    @PersistenceContext(unitName = "ssbd02mpPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UniversityFacade() {
        super(University.class);
    }

    @Override
    @RolesAllowed("getUniversities")
    public University findByName(String name) throws ApplicationException {
        Query q = em.createNamedQuery("University.findByName");
        q.setParameter("name", name);
        try {
            return (University) q.getSingleResult();
        } catch (NoResultException ex) {
            throw UniversityException.createNoUniversityInDbException();
        } catch (Exception e) {
            throw EmployerException.createUnknownException();
        }
    }

}
