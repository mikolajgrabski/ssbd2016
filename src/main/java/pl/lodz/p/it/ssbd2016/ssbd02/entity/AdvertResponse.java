/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Patryk
 */
@Entity
@Table(name = "advert_response")
@NamedQueries({
    @NamedQuery(name = "AdvertResponse.findAll", query = "SELECT a FROM AdvertResponse a"),
    @NamedQuery(name = "AdvertResponse.findByResponseTime", query = "SELECT a FROM AdvertResponse a WHERE a.responseTime = :responseTime"),
    @NamedQuery(name = "AdvertResponse.findByMessage", query = "SELECT a FROM AdvertResponse a WHERE a.message = :message"),
    @NamedQuery(name = "AdvertResponse.findById", query = "SELECT a FROM AdvertResponse a WHERE a.id = :id")})
public class AdvertResponse implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "response_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date responseTime;
    @Size(max = 240)
    @Column(name = "message")
    private String message;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "advert_id", referencedColumnName = "id", updatable = false)
    @ManyToOne(optional = false)
    private Advert advertId;
    @JoinColumn(name = "employee_id", referencedColumnName = "id", updatable = false)
    @ManyToOne(optional = false)
    private Employee employeeId;

    public AdvertResponse() {
    }

    public AdvertResponse(Integer id) {
        this.id = id;
    }

    public AdvertResponse(Integer id, Date responseTime) {
        this.id = id;
        this.responseTime = responseTime;
    }

    public Date getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Date responseTime) {
        this.responseTime = responseTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Advert getAdvertId() {
        return advertId;
    }

    public void setAdvertId(Advert advertId) {
        this.advertId = advertId;
    }

    public Employee getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Employee employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdvertResponse)) {
            return false;
        }
        AdvertResponse other = (AdvertResponse) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.ssbd2016.ssbd02.entity.AdvertResponse[ id=" + id + " ]";
    }
    
}
