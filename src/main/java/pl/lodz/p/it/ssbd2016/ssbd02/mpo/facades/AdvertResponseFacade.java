package pl.lodz.p.it.ssbd2016.ssbd02.mpo.facades;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AbstractFacade;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.AdvertResponse;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AdvertResponseException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

/**
 * @author Patryk Gutenplan
 * Fasada do zarządzania odpowiedziami na ogłoszenie
 */
@Stateless(name = "AdvertResponseFacadeMPO")
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class AdvertResponseFacade extends AbstractFacade<AdvertResponse> implements AdvertResponseFacadeLocal {

    @PersistenceContext(unitName = "ssbd02mpoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AdvertResponseFacade() {
        super(AdvertResponse.class);
    }

    @Override
    @PermitAll
    public void create(AdvertResponse advertResponse) throws ApplicationException {
        try{
            em.persist(advertResponse);
            em.flush();
        }catch (PersistenceException ex){
            if(ex.getMessage().contains("advert_response_unique")) {
                throw AdvertResponseException.createAdvertResponseExistException();
            }
            else {
                throw new AdvertResponseException();
            }
        }

    }
}
