/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mteo.facades;

import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.application.Application;
import javax.interceptor.Interceptors;
import javax.persistence.*;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AbstractFacade;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Advert;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AdvertException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

/**
 *
 * @author Patryk
 */

/**
 * Fasada do operacji na bazie danych dotyczących ogłoszeń.
 */
@Stateless(name = "AdvertFacadeMTEO")
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class AdvertFacade extends AbstractFacade<Advert> implements AdvertFacadeLocal {

    @PersistenceContext(unitName = "ssbd02mteoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AdvertFacade() {
        super(Advert.class);
    }

    @Override
    @RolesAllowed("addAdvert")
    public void create(Advert advert) throws AdvertException {
        try {
            em.persist(advert);
            em.flush();
        } catch (TransactionRequiredException ex) {
            throw AdvertException.createTransactionNotExistException(ex, advert);
        } catch (PersistenceException pe) {
            if(pe.getMessage().contains("title_unique")) {
                throw AdvertException.createNotUniqueAdvertException(pe, advert);
            }
            else {
                throw AdvertException.createUnknownErrorException();
            }
        } catch (Exception e) {
            throw AdvertException.createUnknownErrorException();
        }

    }
    
    @Override
    @RolesAllowed("getMyAdverts")
    public List<Advert> findMyAdverts(Employer employer) throws ApplicationException {
        Query q = em.createNamedQuery("Advert.findByEmployer");
        q.setParameter("employer", employer);
        try {
            return (List<Advert>) q.getResultList();
        } catch (NoResultException ex) {
            throw AdvertException.createNoEntitiesException();
        } catch (Exception e) {
            throw AdvertException.createUnknownErrorException();
        }
    }

    @Override
    @RolesAllowed("editAdvert")
    public void edit(Advert advert) throws ApplicationException {
        try {
            em.merge(advert);
            em.flush();
        } catch(OptimisticLockException oe) {
            throw AdvertException.createAdvertEditionException(oe, advert);
        } catch(TransactionRequiredException ex) {
            throw AdvertException.createTransactionNotExistException(ex, advert);
        } catch(Exception e) {
            throw AdvertException.createUnknownErrorException();
        }
    }

    @Override
    @RolesAllowed("getAdvert")
    public Advert findById(int id) throws ApplicationException {
        Query q = em.createNamedQuery("Advert.findById");
        q.setParameter("id", id);
        try {
            return (Advert) q.getSingleResult();
        } catch (NoResultException ex) {
            throw AdvertException.createNoEntitiesException();
        } catch (Exception e) {
            throw AdvertException.createUnknownErrorException();
        }
    }
}
