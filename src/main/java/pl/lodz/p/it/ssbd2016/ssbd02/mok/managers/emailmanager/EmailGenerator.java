package pl.lodz.p.it.ssbd2016.ssbd02.mok.managers.emailmanager;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Account;

/**
 * Created by Joanna Cichecka on 2016-05-18.
 */
public class EmailGenerator {

    public enum EmailType {
        ACCOUNT_BLOCKED(", Twoje konto na portalu Pracpol.pl zostało zablokowane!"),
        ACCOUNT_UNLOCKED(", Twoje konto na portalu Pracpol.pl zostało odblokowane!"),
        ACCOUNT_CREATED(", Twoja rejestracja na portalu Pracpol.pl przebiegła pomyślnie! ");

        public String info = "";

        EmailType(String info) {
            this.info = info;
        }

        public String getInfo() {
            return info;
        }
    }

    private final static String ACCOUNT_BLOCKED_TITLE = "Zablokowanie konta na " +
            "portalu Pracpol.pl";
    private final static String ACCOUNT_UNLOCKED_TITLE = "Odblokowanie konta na " +
            "portalu z Pracpol.pl";
    private final static String ACCOUNT_CREATED_TITLE = "Witaj na portalu Pracpol.pl!";
    private final static String DEFAULT_TITLE = "Informacja z portalu Pracpol.pl";


    public String generateBody(Account account, EmailType emailType) {
        StringBuilder sb = new StringBuilder();
        sb.append("Witaj, ");
        sb.append(account.getLogin());
        sb.append(emailType.getInfo());

        return sb.toString();
    }

    public String generateTitle(EmailType emailType) {
        switch(emailType) {
            case ACCOUNT_BLOCKED: return ACCOUNT_BLOCKED_TITLE;
            case ACCOUNT_UNLOCKED: return ACCOUNT_UNLOCKED_TITLE;
            case ACCOUNT_CREATED: return ACCOUNT_CREATED_TITLE;
            default: return DEFAULT_TITLE;
        }
    }
}

