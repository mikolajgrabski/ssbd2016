package pl.lodz.p.it.ssbd2016.ssbd02.mok.managers.emailmanager;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Account;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.EmailException;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

/**
 * Created by Joanna Cichecka on 2016-05-18.
 */
@Stateless(name = "pl.lodz.p.it.ssbd2016.ssbd02.mok.managers.emailmanager.EmailManager")
@TransactionAttribute(TransactionAttributeType.MANDATORY)
@Interceptors(LoggingInterceptor.class)
public class EmailManager implements EmailManagerLocal {
    @Resource(lookup = "EmailService")
    private Session mailSession;
    private EmailGenerator emailGenerator = new EmailGenerator();

    private final static String USERNAME = "ssbd02ogloszenia@gmail.com";
    private final static String PASSWORD = "victorinoXmail";
    private final Properties props = new Properties();

    private void setProperties() {
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.starttls.enable", "true");
    }

    private void initMailSession() {
        setProperties();
        mailSession = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(USERNAME, PASSWORD);
            }
        });
    }

    public void sendEmail(Account account, EmailGenerator.EmailType emailType) throws EmailException {
        initMailSession();
        MimeMessage message = new MimeMessage(mailSession);
        try {
            message.setFrom(new InternetAddress("ssbd02ogloszenia@gmail.com"));
            InternetAddress[] address = {new InternetAddress(account.getEmail())};
            message.setRecipients(Message.RecipientType.TO, address);
            message.setSubject(emailGenerator.generateTitle(emailType));
            message.setSentDate(new Date());
            message.setText(emailGenerator.generateBody(account, emailType));
            javax.mail.Transport.send(message);
        }
        catch (AddressException ex) {
            throw EmailException.createWrongEmailException(new Throwable());
        }
        catch (MessagingException ex) {
            throw EmailException.createEmailException(new Throwable());
        }

    }
}
