/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mp.facades;

import java.util.List;
import javax.ejb.Local;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployeeLanguage;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

/**
 *
 * @author Patryk
 */
@Local
public interface EmployeeLanguageFacadeLocal {

    void create(EmployeeLanguage employeeLanguage) throws ApplicationException;

    void edit(EmployeeLanguage employeeLanguage) throws ApplicationException;

    void remove(EmployeeLanguage employeeLanguage);

    EmployeeLanguage find(Object id) throws ApplicationException;

    List<EmployeeLanguage> findAll() throws ApplicationException;

    List<EmployeeLanguage> findRange(int[] range);

    int count();
    
}
