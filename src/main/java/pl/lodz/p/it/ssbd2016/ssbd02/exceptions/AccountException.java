/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.exceptions;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Account;

import javax.faces.context.FacesContext;
import java.util.ResourceBundle;

/**
 *
 * @author piotr
 */

public class AccountException extends ApplicationException {
    
    public static final String KEY_OPTIMISTIC_LOCK_ACCOUNT_EDIT = "ACCOUNT.EXCEPTION.OPTIMISTIC_LOCK_ACCOUNT_EDIT";
    public static final String KEY_PERSISTENCE_EXCEPTION_NOT_UNIQUE_LOGIN =
            "ACCOUNT.EXCEPTION.PERSISTENCE_EXCEPTION_NOT_UNIQUE_LOGIN";
    public static final String KEY_PERSISTENCE_EXCEPTION_NOT_UNIQUE_EMAIL =
            "ACCOUNT.EXCEPTION.PERSISTENCE_EXCEPTION_NOT_UNIQUE_EMAIL";
    public static final String KEY_ACCOUNT_CONCURRENT_MODIFICATION = "ACCOUNT.EXCEPTION" +
            ".ACCOUNT_CONCURRENT_MODIFICATION";
    public static final String KEY_ACCOUNT_CHANGED_AFTER_READING = "ACCOUNT.EXCEPTION.ACCOUNT_CHANGED_AFTER_READING"; 
    public static final String KEY_NO_ENTITY_IN_DB = "ACCOUNT.EXCEPTION.NO_ENTITY_IN_DB";     
    public static final String KEY_NO_ENTITIES_IN_DB = "ACCOUNT.EXCEPTION.NO_ENTITIES_IN_DB";
    public static final String KEY_UNKNOWN_ERROR = "ACCOUNT.EXCEPTION.UNKNOWN_ERROR";
    public static final String KEY_TRANSACTION_NOT_EXISTS = "ACCOUNT.EXCEPTION.TRANSACTION_NOT_EXISTS";
    private static final String KEY_INCORRECT_PASSWORD = "ACCOUNT.EXCEPTION.INCORRECT_PASSWORD";
    public static final String KEY_PROFILE_NOT_CREATED = "ACCOUNT.EXCEPTION.PROFILE_NOT_CREATED";

    private Account account;
    
    public AccountException() {}
    
    public AccountException(String message) {
        super(message);
    }
    
    public AccountException(String message, Throwable cause) {
        super(message, cause);
    }

    public static AccountException createIncorrectPasswordException() {
        return createException(KEY_INCORRECT_PASSWORD);
    }
    
    static public AccountException createAccountEditionAccountException(Throwable cause, Account account) {
        return createException(cause, account, KEY_OPTIMISTIC_LOCK_ACCOUNT_EDIT);
    } 
    
    static public AccountException createNotUniqueLoginAccountException(Throwable cause, Account account) {
        return createException(cause, account, KEY_PERSISTENCE_EXCEPTION_NOT_UNIQUE_LOGIN);
    } 
    
    static public AccountException createNotUniqueEmailAccountException(Throwable cause, Account account) {
        return createException(cause, account, KEY_PERSISTENCE_EXCEPTION_NOT_UNIQUE_EMAIL);
    } 
    
    static public AccountException createConcurentModificationAccountException(Throwable cause, Account account) {
        return createException(cause, account, KEY_ACCOUNT_CONCURRENT_MODIFICATION);
    } 
    
    static public AccountException createChangedAfterReadingAccountException(Throwable cause, Account account) {
        return createException(cause, account, KEY_ACCOUNT_CHANGED_AFTER_READING);
    }

    public static AccountException createTransactionNotExistsException(Throwable cause, Account account) {
        return createException(cause, account, KEY_TRANSACTION_NOT_EXISTS);
    }

    public static AccountException createProfileNotCreatedException(Throwable cause, Account account){
        return createException(cause, account, KEY_PROFILE_NOT_CREATED);
    }
    public static AccountException createUnknownErrorException() {
        return createException(KEY_UNKNOWN_ERROR);
    }
    
    static public AccountException createNoEntityAccountException() {
        return createException(KEY_NO_ENTITY_IN_DB);
    }
    
    static public AccountException createNoEntitiesAccountException() {
        return createException(KEY_NO_ENTITIES_IN_DB);
    }

    private static AccountException createException(Throwable cause, Account account, String msg) {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text",
                FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(msg);

        AccountException accountException = new AccountException(message, cause);
        accountException.setAccount(account);
        return accountException;
    }

    private static AccountException createException(String msg) {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text",
                FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(msg);

        AccountException accountException = new AccountException(message);
        return accountException;
    }

    private void setAccount(Account account) {
        this.account = account;
    }
}
