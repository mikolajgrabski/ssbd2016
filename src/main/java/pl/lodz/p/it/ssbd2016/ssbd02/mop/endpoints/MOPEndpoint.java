package pl.lodz.p.it.ssbd2016.ssbd02.mop.endpoints;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployerRating;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.RatingException;
import pl.lodz.p.it.ssbd2016.ssbd02.mop.facades.EmployeeFacadeLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.mop.facades.EmployerFacadeLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.mop.facades.EmployerRatingFacadeLocal;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.*;
import javax.interceptor.Interceptors;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Joanna Cichecka on 2016-05-07.
 */
/**
 * Endpoint odpowiedzialny za oceny pracodawcy
 */
@Stateful
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class MOPEndpoint implements MOPEndpointLocal, SessionSynchronization {

    private static final Logger logger = Logger.getLogger(MOPEndpoint.class.getName());

    @Resource
    private SessionContext sctx;
    @EJB
    private EmployerRatingFacadeLocal employerRatingFacade;
    @EJB
    private EmployerFacadeLocal employerFacade;
    @EJB
    private EmployeeFacadeLocal employeeFacade;
    private EmployerRating currentEmployerRating;

    private final static boolean NOT_ACCEEPTED = false;
    private long transactionId;

    @Override
    @RolesAllowed("addOpinion")
    public void addOpinion(EmployerRating employerRating) throws ApplicationException {
        employerRating.setEmployeeId(employeeFacade.findByLogin(sctx.getCallerPrincipal().getName()));
        employerRatingFacade.create(employerRating);
    }

    @Override
    @RolesAllowed("acceptOpinion")
    public void acceptOpinion(EmployerRating employerRating) throws ApplicationException {
        Employer ratedEmployer = employerRating.getEmployerId();
        List<EmployerRating> acceptedRatings = employerRatingFacade.findAcceptedByEmployer(ratedEmployer);
        currentEmployerRating = employerRatingFacade.find(employerRating.getId());
        currentEmployerRating.setAccepted(true);
        Float rating = ratedEmployer.getRating() != null ? (ratedEmployer.getRating()*acceptedRatings.size()+
                employerRating.getRating())/(acceptedRatings.size()+1) : employerRating.getRating();
        BigDecimal rounder = new BigDecimal(rating.toString());
        rounder = rounder.setScale(2, BigDecimal.ROUND_HALF_UP);
        rating = rounder.floatValue();
        ratedEmployer.setRating(rating);
        employerRatingFacade.edit(currentEmployerRating);
        employerFacade.edit(ratedEmployer);
        currentEmployerRating = null;
    }

    @Override
    @RolesAllowed("discardOpinion")
    public void discardOpinion(EmployerRating employerRating) throws ApplicationException {
        currentEmployerRating = employerRatingFacade.find(employerRating.getId());
        employerRatingFacade.remove(currentEmployerRating);
        currentEmployerRating = null;
    }

    @Override
    @RolesAllowed("getNotAcceptedOpinions")
    public List<EmployerRating> getNotAcceptedOpinions() throws RatingException {
        return employerRatingFacade.findByAccepted(NOT_ACCEEPTED);
    }

    @Override
    public void afterBegin() throws EJBException, RemoteException {
        transactionId = System.currentTimeMillis();
        logger.log(Level.SEVERE, "Transaction (ID:" + transactionId + ") has " +
                "started by user with ID: " + sctx.getCallerPrincipal().getName() + ".");
    }

    @Override
    public void beforeCompletion() throws EJBException, RemoteException {
        logger.log(Level.SEVERE, "Transaction (ID:" + transactionId + ") " +
                "started by user with ID: " + sctx.getCallerPrincipal().getName() +
                " before completion.");
    }

    @Override
    public void afterCompletion(boolean committed) throws EJBException, RemoteException {
        logger.log(Level.SEVERE, "Transaction (ID:" + transactionId + ") " +
                "started by user with ID: " + sctx.getCallerPrincipal().getName() +
                " was ended because of: " + (committed ? "COMMIT." : "ROLLBACK."));
    }
}
