package pl.lodz.p.it.ssbd2016.ssbd02.entity;

public enum RoleType {
    ADMIN("ADMIN"),
    MODERATOR("MODERATOR"),
    EMPLOYEE("EMPLOYEE"),
    EMPLOYER("EMPLOYER");

    private final String value;

    RoleType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}