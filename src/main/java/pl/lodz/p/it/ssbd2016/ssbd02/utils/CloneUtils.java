/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.utils;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 *
 * @author java
 */
public class CloneUtils {
    
    /**
     * Klasyczna metoda wykonania głębokiej kopii obiektu poprzez jego serializację
     * @param source
     * @return 
     */
    public static Object deepCloneThroughSerialization(Serializable source) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(bos);
        out.writeObject(source);
 
        //De-serialization of object
        ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
        ObjectInputStream in = new ObjectInputStream(bis);
        return in.readObject();
    }

    /**
     * Utworzenie obiektu implementującego interfejs List poprzez refleksję i jego zawartość poprzez serializacje
     * @param listSource
     * @return
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws InstantiationException
     * @throws IOException
     */
    public static Object deepCloneList(List listSource) throws ClassNotFoundException, NoSuchMethodException,
            IllegalAccessException, InvocationTargetException, InstantiationException, IOException {
        Class<List<Serializable>> listClass = (Class<List<Serializable>>) Class.forName(listSource.getClass().getName());
        Constructor<List<Serializable>> listCtr = listClass.getConstructor();
        List<Serializable> outputList = listCtr.newInstance(new Object());

        for(Object obj : listSource){
            Object newInstance = deepCloneThroughSerialization((Serializable) obj);
            outputList.add((Serializable) newInstance);
        }

        return outputList;
    }
}
