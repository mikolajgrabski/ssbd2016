package pl.lodz.p.it.ssbd2016.ssbd02.exceptions;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.University;

import javax.faces.context.FacesContext;
import java.util.ResourceBundle;

/**
 * Created by piotr on 16.05.16.
 */
public class UniversityException extends ApplicationException {
    public static final String KEY_OPTIMISTIC_LOCK_UNIVERSITY_EDITION = "UNIVERSITY.EXCEPTION.OPTIMISTIC_LOCK_UNIVERSITY_EDITION";
    public static final String KEY_PERSISTENCE_EXCEPTION_NOT_UNIQUE_UNIVERSITY= "UNIVERSITY.EXCEPTION.PERSISTENCE_EXCEPTION_NOT_UNIQUE_UNIVERSITY";
    public static final String KEY_NO_UNIVERSITIES_IN_DB = "UNIVERSITY.EXCEPTION.NO_UNIVERSITIES_IN_DB";
    public static final String KEY_TRANSTACTION_NOT_EXIST = "UNIVERSITY.EXCEPTION.TRANSACTION_NOT_EXIST";
    public static final String KEY_UNKNOWN_ERROR = "UNIVERSITY.EXCEPTION.UNKNOWN_ERROR";

    private University university;

    public UniversityException() {}
    public UniversityException(String message) {
        super(message);
    }
    public UniversityException(String message, Throwable cause) {
        super(message, cause);
    }

    public static UniversityException createUniversityEditionException(Throwable cause, University university) {
        return createException(cause, university, KEY_OPTIMISTIC_LOCK_UNIVERSITY_EDITION);
    }

    public static UniversityException createNotUniqueException(Throwable cause, University university) {
        return createException(cause, university, KEY_PERSISTENCE_EXCEPTION_NOT_UNIQUE_UNIVERSITY);
    }

    public static UniversityException createNoUniversityInDbException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());

        return new UniversityException(bundle.getString(KEY_NO_UNIVERSITIES_IN_DB));
    }

    public static UniversityException createTransactionNotExistException(Throwable cause, University university) {
        return createException(cause, university, KEY_TRANSTACTION_NOT_EXIST);
    }

    public static UniversityException createUnknownErrorException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_UNKNOWN_ERROR);

        UniversityException universityException = new UniversityException(message);
        return universityException;
    }

    private static UniversityException createException(Throwable cause, University university, String msg) {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(msg);

        UniversityException universityException = new UniversityException(message, cause);
        universityException.setUniversity(university);
        return universityException;
    }

    private void setUniversity(University university) {
        this.university = university;
    }
}
