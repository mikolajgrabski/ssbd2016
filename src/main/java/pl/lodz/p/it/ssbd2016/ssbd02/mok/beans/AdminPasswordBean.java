package pl.lodz.p.it.ssbd2016.ssbd02.mok.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Account;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Ziarno widoku zmiany hasła dowolnego użytkownika.
 * @author mgrabski
 */
@Named("adminPasswordBean")
@RequestScoped
public class AdminPasswordBean {

    public AdminPasswordBean() {
    }

    @Inject
    private AccountSession accountSession;

    private Account account;
    private String newPassword;
    private String errorMessage;
    private boolean error = false;
    private Text propertiesDict;

    /**
     * Inicjalizacja modelu - metoda w której tworzymy obiekt klasy Text 
     * oraz pobieramy konto do edycji.
     */
    @PostConstruct
    private void initModel(){
        propertiesDict = new Text();
        account = accountSession.getLocalAccountToEdit();
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isError() {
        return error;
    }

    private void setError(boolean error) {
        this.error = error;
    }

    public Account getAccount() {
        return account;
    }

    /**
     * Metoda umożliwiająca zmianę hasła użytkownika.
     */
    public void changeUserPassword() {
        try {
            accountSession.changeUserPassword(account, newPassword);
            addOperationMessage(false, "passwordchanged");
            newPassword = null;
        } catch (AccountException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(AdminPasswordBean.class.getName()).log(Level.SEVERE, null, ex);
            addMessage(true, ex.getMessage());
        }
    }

	/**
	 * Dodanie komunikatu o wyniku operacji
	 * @param isError Sprawdza, czy wystąpił błąd
	 * @param detail Nazwa property, której wartość wyświetlana jest użytkownikowi
	 */
    private void addOperationMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, propertiesDict.getString(detail));
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

	/**
	 * Dodanie komunikatu w przypadku niepowodzenia operacji
	 * @param isError Sprawdza, czy wystąpił błąd
	 * @param detail Wiadomość ze złapanego wyjątku
	 */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
