/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mp.facades;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AbstractFacade;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Account;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.EmployerException;

import java.util.List;

/**
 * Fasada do zarządzania kontem pracodawcy
 * @author Patryk
 */
@Stateless(name = "EmployerFacadeMP")
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class EmployerFacade extends AbstractFacade<Employer> implements EmployerFacadeLocal {

    @PersistenceContext(unitName = "ssbd02mpPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmployerFacade() {
        super(Employer.class);
    }

    @Override
    @PermitAll
    public List<Employer> findAll() throws ApplicationException {
        List<Employer> employers = em.createNamedQuery("Employer.findAll").getResultList();
        if (employers.isEmpty()) {
            throw EmployerException.createNoEmployerInDbException();
        } else
            return employers;
    }

    @Override
    @RolesAllowed({"editEmployer", "acceptProfile"})
    public void edit(Employer employer) throws ApplicationException {
        try {
            em.merge(employer);
            em.flush();
        } catch (OptimisticLockException e) {
            throw EmployerException.createChangedAfterReadingException(e, employer);
        } catch (Exception e) {
            throw EmployerException.createUnknownEmployerException();
        }
    }
}
