package pl.lodz.p.it.ssbd2016.ssbd02.mteo.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Advert;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.AdvertResponse;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.mteo.endpoints.MTEOEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by Piotr Bugara on 06.06.16.
 */

/**
 * Klasa służąca do wyświetlania ogłoszeń danego pracodawcy.
 */
@Named("listMyAdvertBean")
@RequestScoped
public class ListMyAdvertBean {
	
    @EJB
    private MTEOEndpointLocal mteoEndpoint;
    private DataModel<Advert> advertDataModel;
    private Text propertiesDict;
    private String errorMessage;
    private boolean error = false;

    /**
     * Inicjalizuje obiekt, tworząc obiekt klasy Text do wczytywania property
     * oraz pobiera listę wszystkich ogłoszeń z bazy danych, należących do danego pracodawcy.
     */
    @PostConstruct
    public void init() {
        try {
            propertiesDict = new Text();
            this.advertDataModel = new ListDataModel<>(mteoEndpoint.getMyAdverts());
        } catch (ApplicationException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(ListMyAdvertBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Funkcja pobierająca ogłoszenie do edycji, które następnie zapisuje i przekazuje do nowej podstrony.
     *
     * @return "edit-advert", co przekeierowuje na podstronę edit-advert.xhtml
     */
    public String edit() {
        Advert advert = advertDataModel.getRowData();
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("advertToEdit", advert);
        return "edit-advert";
    }

    /**
     * Usuwa ogłoszenie z bazy danych
     */
	public void deleteAdvert(){
		try {
			mteoEndpoint.deleteAdvert(advertDataModel.getRowData());
			addOperationMessage(false, "advertdeleted");
		} catch (ApplicationException ex) {
			setError(true);
			setErrorMessage(ex.getMessage());
			Logger.getLogger(ListMyAdvertBean.class.getName()).log(Level.SEVERE, null, ex);
			addMessage(true, ex.getMessage());
		}
	}

	/**
	 * Przygotowuje widok z listą opowiedzi na wybrane ogłoszenie.
	 * Przekazuje do FacesContext listę odpowiedzi.
	 * @return Przekierowuje do odpowiedniego formularza xhtml.
	 */
    public String prepareResponseListView() {
    	List<AdvertResponse> advertResponseList = advertDataModel.getRowData().getAdvertResponseList();
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("advertResponseList", advertResponseList);
        return "advert-response-list";
    }

    /**
     * Dodanie komunikat o wyniku operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Nazwa property, której wartość wyświetlana jest użytkownikowi
     */
    private void addOperationMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, propertiesDict.getString(detail));
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Dodanie komunikatu w przypadku niepowodzenia operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Wiadomość ze złapanego wyjątku
     */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public DataModel<Advert> getAdvertDataModel() {
        return advertDataModel;
    }
    public void setAdvertDataModel(DataModel<Advert> advertDataModel) {
        this.advertDataModel = advertDataModel;
    }
    public String getErrorMessage() {
        return errorMessage;
    }
    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    public boolean isError() {
        return error;
    }
    private void setError(boolean error) {
        this.error = error;
    }
}
