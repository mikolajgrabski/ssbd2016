package pl.lodz.p.it.ssbd2016.ssbd02.mok.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Account;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Ziarno widoku zmiany własnego hasła.
 * @author mgrabski
 */

@Named("changePasswordPageBean")
@RequestScoped
public class ChangePasswordPageBean {

    @Inject
    private AccountSession accountSession;

    private Account account = new Account();
    private String oldPassword;
    private String newPassword;
    private String confirmNewPassword;
    private String errorMessage;
    private Text propertiesDict;
    private boolean error = false;

    /**
     * Domyślny konstruktor, w którym inicjalizowana jest zmienna propertiesDict.
     */
    public ChangePasswordPageBean() {
        propertiesDict = new Text();
    }

    /**
     * Inicjalizacja modelu - metoda, w której pobierane jest konto zalogowanego
     * użytkownika
     */
    @PostConstruct
    public void initModel() {
        account = accountSession.getCurrentAccount();
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isError() {
        return error;
    }

    private void setError(boolean error) {
        this.error = error;
    }

    public Account getAccount() {
        return account;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    public void setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
    }
    
    /**
     * Metoda umożliwiająca zmianę własnego hasła.
     * @throws AccountException
     */
    public void changePassword() throws AccountException {
        try {
            accountSession.changePassword(oldPassword, newPassword);
            addMessage(false, "");
        } catch (ApplicationException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(EditPageBean.class.getName()).log(Level.SEVERE, null, ex);
            addMessage(true, ex.getMessage());
        }
    }

	/**
	 * Dodanie komunikatu o wyniku operacji
	 * @param isError Sprawdza, czy wystąpił błąd
	 * @param detail Nazwa property, której wartość wyświetlana jest użytkownikowi
	 */
    private void addOperationMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, propertiesDict.getString(detail));
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

	/**
	 * Dodanie komunikatu w przypadku niepowodzenia operacji
	 * @param isError Sprawdza, czy wystąpił błąd
	 * @param detail Wiadomość ze złapanego wyjątku
	 */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

}
