package pl.lodz.p.it.ssbd2016.ssbd02.mop.endpoints;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployerRating;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.RatingException;

import javax.annotation.security.RolesAllowed;
import java.util.List;

public interface MOPEndpointLocal {
    /**
     * Dodaje opinie o pracodawcy
     * @param employerRating Ocena pracodawcy
     * @throws ApplicationException
     */
    void addOpinion(EmployerRating employerRating) throws ApplicationException;
    /**
     * Akceptacja istniejącej opini niezatwierdzonej
     * @param employerRating Ocena pracodawcy
     * @throws ApplicationException
     */
    void acceptOpinion(EmployerRating employerRating) throws ApplicationException;
    /**
     * Odrzucenie istniejącej opini
     * @param employerRating Opinia pracodawcy
     * @throws ApplicationException
     */
    void discardOpinion(EmployerRating employerRating) throws ApplicationException;

    /**
     * Zwraca listę niezaakceptowanych opinii
     * @return lista wszystkich niezaakceptowanych opinii
     * @throws RatingException
     */
    List<EmployerRating> getNotAcceptedOpinions() throws RatingException;
}
