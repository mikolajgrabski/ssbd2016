/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mos.facades;

import java.util.List;
import javax.ejb.Local;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.City;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.CityException;

/**
 *
 * @author Patryk
 */
@Local
public interface CityFacadeLocal {

    void create(City city) throws ApplicationException;

    void edit(City city) throws ApplicationException;

    void remove(City city);

    City find(Object id) throws ApplicationException;

    List<City> findAll() throws ApplicationException;

    List<City> findRange(int[] range);

    int count();

    /**
     * Przeprowadza wyszukiwanie miasta po nazwie.
     * @param name nazwa miasta
     * @return odnalezione miasto
     * @throws CityException
     */
    City findByName(String name) throws CityException;

    /**
     * Przeprowadza wyszukiwanie miasta po id
     * @param id id miasta
     * @return odnalezione miasto
     * @throws CityException
     */
    City findById(Object id) throws CityException;
    
}
