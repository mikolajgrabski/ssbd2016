package pl.lodz.p.it.ssbd2016.ssbd02.mok.endpoints;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AccessLevel;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Account;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.EmailException;

import javax.ejb.Local;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.List;

/**
 * Interfejs zapewniający wszystkie funkcjonalności MOK.
 * @author mgrabski
 *
 */

@Local
public interface MOKEndpointLocal {

    /**
     * Aktywuje wybrane konto.
     * @param account Konto do aktywacji
     * @throws AccountException
     */
    void activateAccount(Account account) throws AccountException, EmailException;
    /**
     * Deaktywuje wybrane konto.
     * @param account Konto do deaktywacji
     * @throws AccountException
     */
    void deactivateAccount(Account account) throws AccountException, EmailException;

    /**
     * Pozwala użytkownikowi utworzyc wlasne konto. Ustawia dla konta jeden z 
     * dwoch wybranych poziomow dostepu (employer/employee). Tworzy domyslny 
     * profil uzytkownika.
     * @param account Tworzone konto użytkownika
     * @throws AccountException
     */
    void registerAccount(Account account) throws AccountException, EmailException;

    /**
     * Pozwala administratorowi utworzyc nowe konto użytkownika. Ustawia 
     * dla konta dowolna liste poziomow dostepu. Tworzy domyslny profil 
     * uzytkownika.
     * @param account Tworzone konto użytkownika
     * @throws AccountException
     */
    void createAccount(Account account) throws AccountException;

    /**
     * Zwraca uzytkownikowi jego konto.
     * @return Zwraca obiekt typu Account.
     * @throws AccountException
     */
    Account getMyAccount() throws AccountException;

    /**
     * Zwraca konto dowolnie wybranego uzytkownika.
     * @param account Wybrane konto użytkownika
     * @return Zwraca obiekt typu Account.
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws AccountException
     */
    Account getCurrentAccount(Account account) throws IOException, ClassNotFoundException, AccountException;

    /**
     * Zwraca listę poziomów dostępu przypisanych do danego konta.
     * @param account Wybrane konto użytkownika
     * @return Zwraca listę obiektów typu AccessLevel
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws IOException
     * @throws InvocationTargetException
     * @throws ClassNotFoundException
     * @throws AccountException
     */
    Collection<AccessLevel> getCurrentAccessLevels(Account account) throws NoSuchMethodException,
            IllegalAccessException, InstantiationException, IOException,
            InvocationTargetException, ClassNotFoundException, AccountException;

    /**
     * Zmienia listę poziomów dostępu dla wybranego konta użytkownika.
     * @param account Wybrane konto użytkownika
     * @throws AccountException
     */
    void changeAccessLevels(Account account) throws AccountException;

    /**
     * Pobiera i zwraca listę wszystkich kont.
     * @return Zwaraca listę obiektów typu Account
     * @throws AccountException
     */
    List<Account> getAllAccounts() throws AccountException;

    /**
     * Zmienia hasło zalogowanego użytkownika. Weryfikuje czy hash podanego hasła jest zgodny
     * z obecnym hasłem użytkownika, następnie zmienia hasło w bazie na hash nowego hasła.
     * @param oldPassword Obecne hasło użytkownika
     * @param newPassword Nowe hasło użytkownika
     * @throws AccountException
     */
    void changePassword(String oldPassword, String newPassword) throws AccountException;

    /**
     * Umożliwia edycję adresu e-mail dla danego konta.
     * @param oldAccount Konto użytkownika sprzed edycji
     * @param newAccount Konto użytkownika ze zmienionymi danymi
     * @throws AccountException
     */
    void editAccount(Account oldAccount, Account newAccount) throws AccountException;

    /**
     * Umożliwia zmianę hasła dla dowolnego użytkownika bez konieczności podania starego hasła.
     * Nowe hasło jest hashowane przed wywołaniem funkcji edit z fasady konta.
     * @param account Konto na którym dokonywana jest zmiana hasła
     * @param newPassword Nowe hasło użytkownika
     * @throws AccountException
     */
    void changeUserPassword(Account account, String newPassword) throws AccountException;

    /**
     * Umożliwia pobranie konta użytkownika o określonym loginie.
     * @param login Login poszukiwanego konta
     * @return Zwraca obiekty typu Account
     * @throws AccountException
     */
    Account getAccountByLogin(String login) throws AccountException;

    void editLoginStats(Account account) throws AccountException, EmailException;
}
