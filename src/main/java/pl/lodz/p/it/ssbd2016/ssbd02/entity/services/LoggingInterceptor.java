package pl.lodz.p.it.ssbd2016.ssbd02.entity.services;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import java.util.logging.Logger;

/**
 * Created by Joanna Cichecka on 2016-04-23.
 */
public class LoggingInterceptor {
    private static final Logger logger =
            Logger.getLogger(LoggingInterceptor.class.getName());
    @Resource
    private SessionContext sessionContext;

    @AroundInvoke
    public Object traceInvoke(InvocationContext invocationContext) throws
            Exception {
        String className = invocationContext.getTarget().getClass()
                .getCanonicalName();
        String methodName = invocationContext.getMethod().getName();
        String person = sessionContext.getCallerPrincipal().getName();

        StringBuilder parameters = new StringBuilder();
        if(invocationContext.getParameters() != null) {
            for(Object p : invocationContext.getParameters()) {
                if(parameters.length() > 0) {
                    parameters.append(", ");
                }
                parameters.append(p);
            }
        }

        logger.info(className + "." + methodName + "(" + parameters + ") has " +
                "been invoked by " + person);

        Object result;
        try {
            result = invocationContext.proceed();
        } catch (Exception e) {
            StringBuilder causes = new StringBuilder();

            Throwable cause = e.getCause();
            while(cause != null) {
                if(causes.length() > 0) {
                    causes.append(", ");
                }
                causes.append(cause);
                cause = cause.getCause();
            }

            logger.severe(className + "." + methodName + "(" + parameters +
                    ") invoked by " + person + " has thrown an exception " +
                    e + ", " + e.getLocalizedMessage() + " with causes: " +
                    causes);
            throw e;
        }

        String resultInfo = invocationContext.getMethod().getReturnType()
                .getCanonicalName();
        logger.info(className + "." + methodName +  "(" + parameters + ") " +
                "invoked by " + person + " has returned " + resultInfo);
        return result;

    }
}
