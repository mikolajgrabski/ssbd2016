package pl.lodz.p.it.ssbd2016.ssbd02.mteo.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.*;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.mteo.endpoints.MTEOEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Piotr Bugara on 2016-06-01.
 */

/**
 * Klasa służąca do tworzenie nowego ogłoszenia.
 */
@Named("createAdvertBean")
@RequestScoped
public class CreateAdvertBean {
    @EJB
    private MTEOEndpointLocal mteoEndpoint;

    private Advert advert;
    private List<Category> categoryList;
    private List<String> categories;
    private String cityName;
    private String errorMessage;
    private boolean error = false;
    private Text propertiesDict;

    /**
     * Domyślny konstruktor. Inicjalizuje zmienną propertiesDict
     */
    public CreateAdvertBean() {
        propertiesDict = new Text();
    }

    /**
     * Inicjalizuje klasę. Ustawia domyślną wartość dla liczby odpowiedzi
     * na ogłoszenie, a także pobiera wszystkie kategorie
     * z bazy danych w celu wyświetlenia menu wyboru.
     */
    @PostConstruct
    public void init() {
        try {
            advert = new Advert();
            advert.setResponsesLeft(Short.parseShort("10"));
            categoryList = mteoEndpoint.getAllCategories();
        } catch (ApplicationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Tworzy nowe ogłoszenie
     * @throws AccountException
     */
    public void create() throws AccountException {
        try {

            mteoEndpoint.addAdvert(advert, cityName, categories);
            addOperationMessage(false, "advertadded");
        } catch (ApplicationException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(CreateAdvertBean.class.getName()).log(Level.SEVERE, null, ex);
            addMessage(true, ex.getMessage());
        }
    }

    /**
     * Dodanie komunikat o wyniku operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Nazwa property, której wartość wyświetlana jest użytkownikowi
     */
    private void addOperationMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, propertiesDict.getString(detail));
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Dodanie komunikatu w przypadku niepowodzenia operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Wiadomość ze złapanego wyjątku
     */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public Advert getAdvert() {
        return advert;
    }
    public void setAdvert(Advert advert) {
        this.advert = advert;
    }
    public List<Category> getCategoryList() {
        return categoryList;
    }
    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    public String getCityName() {
        return this.cityName;
    }
    public void setCategories(List<String> categories) {
        this.categories = categories;
    }
    public List<String> getCategories() {
        return this.categories;
    }
    public String getErrorMessage() {
        return errorMessage;
    }
    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    public boolean isError() {
        return error;
    }
    private void setError(boolean error) {
        this.error = error;
    }
}
