/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Patryk
 */
@Entity
@Table(name = "employee_job_history")
@NamedQueries({
        @NamedQuery(name = "EmployeeJobHistory.findAll", query = "SELECT e FROM EmployeeJobHistory e"),
        @NamedQuery(name = "EmployeeJobHistory.findById", query = "SELECT e FROM EmployeeJobHistory e WHERE e.id = " +
                ":id"),
        @NamedQuery(name = "EmployeeJobHistory.findByPosition", query = "SELECT e FROM EmployeeJobHistory e WHERE e" +
                ".position = :position"),
        @NamedQuery(name = "EmployeeJobHistory.findByEmployer", query = "SELECT e FROM EmployeeJobHistory e WHERE e" +
                ".employer = :employer"),
        @NamedQuery(name = "EmployeeJobHistory.findByDateFrom", query = "SELECT e FROM EmployeeJobHistory e WHERE e" +
                ".dateFrom = :dateFrom"),
        @NamedQuery(name = "EmployeeJobHistory.findByDateTo", query = "SELECT e FROM EmployeeJobHistory e WHERE e" +
                ".dateTo = :dateTo")})
public class EmployeeJobHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Size(min = 1, max = 30)
    @Column(name = "position")
    private String position;
    @Basic(optional = false)
    @Size(min = 1, max = 30)
    @Column(name = "employer")
    private String employer;
    @Basic(optional = false)
    @Column(name = "date_from")
    @Temporal(TemporalType.DATE)
    private Date dateFrom;
    @Column(name = "date_to")
    @Temporal(TemporalType.DATE)
    private Date dateTo;
    @JoinColumn(name = "employee_id", referencedColumnName = "id", updatable = false)
    @ManyToOne(optional = false)
    private Employee employeeId;

    public EmployeeJobHistory() {
    }

    public EmployeeJobHistory(Employee employee) {
        this.employeeId = employee;
    }

    public EmployeeJobHistory(Integer id, String position, String employer, Date dateFrom) {
        this.id = id;
        this.position = position;
        this.employer = employer;
        this.dateFrom = dateFrom;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Employee getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Employee employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmployeeJobHistory)) {
            return false;
        }
        EmployeeJobHistory other = (EmployeeJobHistory) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployeeJobHistory[ id=" + id + " ]";
    }

}
