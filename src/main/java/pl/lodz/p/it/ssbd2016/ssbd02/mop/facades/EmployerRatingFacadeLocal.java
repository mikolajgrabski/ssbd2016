package pl.lodz.p.it.ssbd2016.ssbd02.mop.facades;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployerRating;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.RatingException;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Local;
import java.util.List;

@Local
public interface EmployerRatingFacadeLocal {

    /**
     * Dodawanie opini o pracodawcy do bazy
     * @param employerRating
     * @throws ApplicationException
     */
    void create(EmployerRating employerRating) throws ApplicationException;

    void edit(EmployerRating employerRating) throws ApplicationException;

    void remove(EmployerRating employerRating);

    EmployerRating find(Object id) throws ApplicationException;

    List<EmployerRating> findAll() throws ApplicationException;

    List<EmployerRating> findRange(int[] range);

    /**
     * Zwraca listę zaakceptowanych/niezaakceptowanych ocen
     * @param accepted czy ocena ma być zaakceptowana - true/false
     * @return lista ocen
     * @throws RatingException
     */
    List<EmployerRating> findByAccepted(boolean accepted) throws RatingException;

    int count();

    /**
     * Zwraca listę zaakceptowanych ocen dla danego pracodawcy
     * @param employer pracodawca
     * @return lista zaakceptowanych ocen dla danego pracodawcy
     * @throws RatingException
     */
    List<EmployerRating> findAcceptedByEmployer(Employer employer) throws RatingException;
}
