package pl.lodz.p.it.ssbd2016.ssbd02.exceptions;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AdvertResponse;

import javax.faces.context.FacesContext;
import java.util.ResourceBundle;

public class AdvertResponseException extends ApplicationException {
    public static final String KEY_PERSISTENCE_EXCEPTION_NO_RESPONSE_LEFT = "ADVERT_RESPONSE.EXCEPTION.NO_RESPONSE_LEFT";

    public static final String KEY_PERSISTENCE_EXCEPTION_NOT_UNIQUE_ADVERT_RESPONSE = "ADVERT_RESPONSE.EXCEPTION.PERSISTENCE_EXCEPTION_NOT_UNIQUE_ADVERT_RESPONSE";
    public static final String KEY_PERSISTENCE_EXCEPTION_DATE = "ADVERT_RESPONSE.EXCEPTION.PERSISTENCE_EXCEPTION_WRONG_DATE";

    private AdvertResponse advertResponse;

    public AdvertResponseException() { }

    public AdvertResponseException(String message) {
        super(message);
    }

    public AdvertResponseException(String message, Throwable cause) {
        super(message, cause);
    }

    private static AdvertResponseException createAdvertException(Throwable cause, AdvertResponse advertResponse, String msg) {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(msg);

        AdvertResponseException advertResponseException = new AdvertResponseException(message);
        advertResponseException.setAdvertResponse(advertResponse);
        return advertResponseException;
    }

    private void setAdvertResponse(AdvertResponse advertResponse) {
        this.advertResponse = advertResponse;
    }

    public static AdvertResponseException createNoResponseLeftAdvertException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_PERSISTENCE_EXCEPTION_NO_RESPONSE_LEFT);

        AdvertResponseException advertResponseException = new AdvertResponseException(message);
        return advertResponseException;
    }

    public static AdvertResponseException createAdvertResponseExistException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_PERSISTENCE_EXCEPTION_NOT_UNIQUE_ADVERT_RESPONSE);

        AdvertResponseException advertResponseException = new AdvertResponseException(message);
        return advertResponseException;
    }

    public static AdvertResponseException createAdvertResponseDateException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_PERSISTENCE_EXCEPTION_DATE);

        AdvertResponseException advertResponseException = new AdvertResponseException(message);
        return advertResponseException;
    }

}
