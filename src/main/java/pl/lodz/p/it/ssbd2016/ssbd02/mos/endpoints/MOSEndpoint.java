package pl.lodz.p.it.ssbd2016.ssbd02.mos.endpoints;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.City;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.University;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.CityException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.UniversityException;
import pl.lodz.p.it.ssbd2016.ssbd02.mos.facades.CityFacadeLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.mos.facades.UniversityFacadeLocal;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.*;
import javax.interceptor.Interceptors;
import java.rmi.RemoteException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Joanna Cichecka on 2016-05-07.
 */
@Stateful
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class MOSEndpoint implements MOSEndpointLocal, SessionSynchronization {

    private static final Logger logger = Logger.getLogger(MOSEndpoint.class.getName());

    @Resource
    private SessionContext sctx;
    @EJB
    private CityFacadeLocal cityFacade;
    @EJB
    private UniversityFacadeLocal universityFacade;

    private City currentCity;
    private long transactionId;

    @Override
    @RolesAllowed("addCity")
    public void addCity(City city) throws ApplicationException {
        cityFacade.create(city);
    }

    /**
     * Metoda wykonujaa edycje miasta
     * @param newCity Miasto po zmianach
     * @param oldCity Miasto przed zmianamu
     * @throws ApplicationException
     */
    @Override
    @RolesAllowed("editCity")
    public void editCity(City newCity, City oldCity) throws ApplicationException {
        oldCity.setName(newCity.getName());
        cityFacade.edit(oldCity);
    }

    @Override
    @RolesAllowed("getCities")
    public List<City> getCities() throws ApplicationException {
        return cityFacade.findAll();
    }

    @Override
    @RolesAllowed("addUniversity")
    public void addUniversity(University university) throws ApplicationException {
        university.setCityId(getOrCreateCity(university.getCityId().getName()));
        universityFacade.create(university);
    }

    @Override
    @RolesAllowed("editUniversity")
    public void editUniversity(University newUniversity, University oldUniversity) throws ApplicationException {
        oldUniversity.setCityId(getOrCreateCity(newUniversity.getCityId().getName()));
        oldUniversity.setName(newUniversity.getName());
        universityFacade.edit(oldUniversity);
    }

    @Override
    @RolesAllowed("addUniversity")
    public City getCity(String name) throws CityException {
        return cityFacade.findByName(name);
    }

    @Override
    @RolesAllowed("editEmployee")
    public University findUniversityByName(String name) throws UniversityException {
        return universityFacade.findByName(name);
    }
    @RolesAllowed("addCity")
    public City getOrCreateCity(String cityName) throws ApplicationException {
        if (cityFacade.findByName(cityName) == null) {
            cityFacade.create(new City(cityName));
        }
        return cityFacade.findByName(cityName);
    }

    @Override
    @RolesAllowed("getUniversities")
    public University getUniversityById(Short id) throws UniversityException {
        return universityFacade.findById(id);
    }

    @Override
    @RolesAllowed("getUniversities")
    public List<University> getUniversities() throws ApplicationException {
        return universityFacade.findAll();
    }

    /**
     * Loguje informacje o rozpoczęciu transakcji
     * @throws EJBException
     * @throws RemoteException
     */
    @Override
    public void afterBegin() throws EJBException, RemoteException {
        transactionId = System.currentTimeMillis();
        logger.log(Level.SEVERE, "Transaction (ID:" + transactionId + ") has " +
                "started by user with ID: " + sctx.getCallerPrincipal().getName() + ".");
    }

    /**
     * Loguje informacje przed zakończeniem transakcji
     * @throws EJBException
     * @throws RemoteException
     */
    @Override
    public void beforeCompletion() throws EJBException, RemoteException {
        logger.log(Level.SEVERE, "Transaction (ID:" + transactionId + ") " +
                "started by user with ID: " + sctx.getCallerPrincipal().getName() +
                " before completion.");
    }

    /**
     * Loguje informacje o zakończonej transakcji
     * @param committed Informacja o zatwierdzeniu (true) lub przerwaniu transakcji (false)
     * @throws EJBException
     * @throws RemoteException
     */
    @Override
    public void afterCompletion(boolean committed) throws EJBException, RemoteException {
        logger.log(Level.SEVERE, "Transaction (ID:" + transactionId + ") " +
                "started by user with ID: " + sctx.getCallerPrincipal().getName() +
                " was ended because of: " + (committed ? "COMMIT." : "ROLLBACK."));
    }

}
