/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mp.facades;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Category;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

import javax.ejb.Local;
import java.util.List;

/**
 * @author Patryk
 */
@Local
public interface CategoryFacadeLocal {

    void create(Category category) throws ApplicationException;

    void edit(Category category) throws ApplicationException;

    void remove(Category category);

    Category find(Object id) throws ApplicationException;

    List<Category> findAll() throws ApplicationException;

    List<Category> findRange(int[] range);

    /**
     * Zwraca kategorię dla podanej nazwy
     * @param name nazwa kategorii
     * @return kategoria o podanej nazwie
     * @throws ApplicationException
     */
    Category findByName(String name) throws ApplicationException;

    int count();

}
