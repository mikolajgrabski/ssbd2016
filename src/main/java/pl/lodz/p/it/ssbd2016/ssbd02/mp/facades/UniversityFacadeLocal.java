/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mp.facades;

import java.util.List;
import javax.ejb.Local;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.University;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

/**
 *
 * @author Patryk
 */
@Local
public interface UniversityFacadeLocal {

    void create(University university) throws ApplicationException;

    void edit(University university) throws ApplicationException;

    void remove(University university);

    University find(Object id) throws ApplicationException;

    List<University> findAll() throws ApplicationException;

    List<University> findRange(int[] range);

    int count();

    /**
     * Wyszukuje uniwersytet po jego nazwie
     * @param name nazwa uniwersytetu
     * @return uniwersytet o podanej nazwie
     * @throws ApplicationException
     */
    University findByName(String name) throws ApplicationException;
}
